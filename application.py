from flask import Flask
from urllib.parse import quote_plus as urlquote 

app = Flask(__name__)
#Connect to the MSSQL database
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://pcdbadmin:%s@pcdb33.cpkiog7t5lob.us-east-1.rds.amazonaws.com/PCDB' % urlquote('~#RKkR&X5MP[B3c@')
#Security key - should probably change it to something more secure
app.config['SECRET_KEY'] = 'supersecretkey'
app.debug=True

#import all views
from views import *

#Runs the app
if __name__ == '__main__':
   app.run(host="0.0.0.0")
   
   