var editopstable;
var editsampstable; 

function newprodfunc(){
    $("#addpform").show();
    $("#addproduct").show();
    $("#cancel").show();
    };

function cancelnewprod(){
    $("#addpform").hide();
    $("#addproduct").hide();
    $("#cancel").hide();
    };
  
function backtoprod() {
     $("#productpage").show();
     $("#opspage").hide();
     };

function primrbacktoops() {
     $("#primrpage").hide();
     $("#opspage").show();
     };

function parambacktoops() {
     $("#paramspage").hide();
     $("#opspage").show();
     };
     
function newoperation(){
    $("#newopform").show();
    $("#addoperation").show();
    $("#cancelop").show();
    }; 

function cancelnewoperation(){
    $("#newopform").hide();
    $("#addoperation").hide();
    $("#cancelop").hide();
    };

function newsample(){
    $("#newsampform").show();
    $("#addsample").show();
    $("#cancelsamp").show();
    };
    
function cancelnewsample(){
    $("#newsampform").hide();
    $("#addsample").hide();
    $("#cancelsamp").hide();
    };

function newalias(){
    $("#addaform").show();
    $("#cancelalias").show();
    $("#addalias").show();
    };
  
function cancelnewalias(){
    $("#addaform").hide();
    $("#cancelalias").hide();  
    $("#addalias").hide();
    };

function newprocess(){
    $("#processform").show();
    $("#addprocess").show();
    $("#cancelprocess").show();
    };
  
function cancelnewprocess(){
    $("#processform").hide();
    $("#addprocess").hide();
    $("#cancelprocess").hide();
    };

function gotoops() {
      $("#productpage").hide();
      $("#opspage").show();
      $("#backtoprod").show();
      getmeasures();
      }; 

function secretmeasure(){
    $("#newmeasure").show();
    $("#editallmeasure").show();
    $("#hideallmeasure").show();
    };
    
function newmeasure(){
    $("#newmeasform").show();
    $("#cancelmeas").show();
    $("#addnewmeas").show();
};    
    
function cancelnewmeas(){
    $("#newmeasform").hide();
    $("#cancelmeas").hide();
    $("#addnewmeas").hide();
    };

function hideallmeasures(){
    $("#newmeasure").hide();
    $("#editallmeasure").hide();
    $("#hideallmeasure").hide();
};

function deltavbacktoops() {
     $("#deltavpage").hide();
     $("#opspage").show();
     };
     
function dvpibacktoops() {
     $("#dvpipage").hide();
     $("#opspage").show();
     };

function editopsbacktoops() {
     getoperations();
     $("#editopspage").hide();
     $("#opspage").show();
     };

function editsampsbacktoops() {
     getsamples();
     $("#editsampspage").hide();
     $("#opspage").show();
     };

function editmeasbacktoops() {
     $("#editmeaspage").hide();
     $("#newmeasure").hide();
     $("#editallmeasure").hide();
     $("#hideallmeasure").hide();
     $("#opspage").show();
     getmeasures();
     };
     
function lesbacktoops() {
     $("#lespage").hide();
     $("#opspage").show();
     };     
     
$(function() {
    $("#product_select").on('change', function(event) {
    $("#process_select").show();
    $("#output").hide();
    $("#process_table").hide();
    $("#gotoops").hide();
    getprocesses();    
    $("#newprocess").show();
    makealtable();  
    makeprodtable(); 
         });
      });

$(function() {
    $("#process_select").on('change', function(event) {
    $("#process_table").show();
    getoperations();  
    $("#gotoops").show();
    $("#editallmeasure").hide();
    $("#sample_select").empty();
    $("#sample_select").hide();
    $("#gotoeditsamps").hide();
    $("#newsample").hide();        
    $("#measure_select").hide();
    $("#addmeasures").hide();
    $("#measure_table").hide();
    $("#currmeas").hide();
    $("#newmeasure").hide();
    makeproctable(); 
         });
      });
 
$(function() {
     $("#operation_select").on('change', function(event) {
     $("#sample_select").empty();
     $("#sample_select").show();
     $("#gotoeditsamps").show();
     $("#newsample").show();
     $("#editallmeasure").hide();
     $("#measure_select").hide();
     $("#addmeasures").hide();
     $("#currmeas").hide();
     $("#newmeasure").hide();
     $("#measure_table").hide();
     updatemeasures();
     $("#sample_select").append($('<option>Loading..</option>').val('null'));
     getsamples(); 
      });
      });

$(function() {
     $("#sample_select").on('change', function(event) {
     $("#measure_select").show();
     $("#addmeasures").show();
     $("#measure_table").show();
     $("#currmeas").show();
         updatemeasures(); 
        })
      });
      
$(function() {
    getbiibs();
});

function getbiibs(){
$("#product_select").append($('<option>Loading..</option>').val('null'));
     $.ajax({
          data : {
             biib_choices :  'biib_choices',
                 },
             type : 'GET',
             url : '/getbiibs'

            })
     .done(function(data) {
          $("#product_select").empty();
          $("#product_select").append($('<option>Select a Product</option>').val('null'));
       
        var ele = document.getElementById("product_select"); 
        for (var i = 0; i < data.biib_choices.length; i++) {
            ele.innerHTML = ele.innerHTML +
    '<option value="' + data.biib_choices[i]['biib_code'] + '">' + data.biib_choices[i]['biib_code'] + '</option>'; };
              
})
event.preventDefault();
};

function getprocesses(){
     $.ajax({
          data : {
             biib_code : $("#product_select").val()
                 },
             type : 'POST',
             url : '/getprocess'
            })
            
        .done(function(data) {
          $("#output").text(data.output).show();
          $("#process_select").empty();
          $("#process_select").append($('<option>Select a Process</option>').val('null'));
        var ele = document.getElementById("process_select"); 
        for (var i = 0; i < data.pro_choices.length; i++) {
            ele.innerHTML = ele.innerHTML +
    '<option value="' + data.pro_choices[i]['id'] + ")" + data.pro_choices[i]['mfg_mode'] + "-" + data.pro_choices[i]['process_identifier'] + '">' + data.pro_choices[i]['biib_code'] + ": " + data.pro_choices[i]['mfg_mode'] 
    + "-" + data.pro_choices[i]['process_identifier'] + " (" +  data.pro_choices[i]['process_status'] + ")" + '</option>';       };
    
    });
    }; 

function getoperations(){
    $.ajax({
          data : {
             biib_code : $("#product_select").val(),
             pro_id :  $("#process_select").val(),
                 },
             type : 'POST',
             url : '/getoperation'
            })
             
        .done(function(data) {
          $("#currentprocess").html(data.currentprocess).show();
          $("#operation_select").empty();
          $("#operation_select").append($('<option>Select an Operation</option>').val('null'));

        var ele = document.getElementById("operation_select"); 
        for (var i = 0; i < data.op_choices.length; i++) {
            ele.innerHTML = ele.innerHTML +
 '<option value="' + data.op_choices[i]['operation_id'] + "|" + data.op_choices[i]['operation_type'] + '">' + data.op_choices[i]['operation_order'] + "-" + data.op_choices[i]['operation_full_name'] + '</option>'; };
 
    });
    }; 

function getsamples(){
     $.ajax({
          data : {
             operation_type : $("#operation_select").val(),
                 },
             type : 'POST',
             url : '/getsample'
            })
            
        .done(function(data) {
          $("#sample_select").empty();
          $("#sample_select").append($('<option>Select a Sample</option>').val('null'));

        var ele = document.getElementById("sample_select"); 
        for (var i = 0; i < data.samp_choices.length; i++) {
            ele.innerHTML = ele.innerHTML +
 '<option value="' + data.samp_choices[i]['sample_id'] + '">' + data.samp_choices[i]['sample_order'] + "-" + data.samp_choices[i]['sample_name'] + '</option>'; };
 
        });
      };
      
function getmeasures() {
      $("#measure_select").empty();
      $("#measure_select").append($('<option>Loading..</option>').val('null'));
      $.ajax({
          data : {
             meas_choices :  'meas_choices',
                 },
             type : 'GET',
             url : '/getmeasures'

            })
     .done(function(data) {
        $("#measure_select").empty();  
        var ele = document.getElementById("measure_select"); 
        for (var i = 0; i < data.meas_choices.length; i++) {
            ele.innerHTML = ele.innerHTML +
    '<option value="' + data.meas_choices[i]['measure_id'] + '">' + data.meas_choices[i]['measure_name'] + " (" + data.meas_choices[i]['units_of_measure'] + ")" + '</option>'; };

      });
      };
      
function addproduct() {
       $("#addpform").hide();
        $("#addproduct").hide();
        $("#cancel").hide();
       $.ajax({
          data : {
             biib_code_new : $("#biib_code_new").val(),
             partner: $("#partner").val(),
             modality: $("#modality").val(),
                 },
             type : 'POST',
             url : '/addproduct'
            })
        .done(function(data) {
        alert(data.output);
            getbiibs();    
      });
      event.preventDefault();
      };
      
function addalias() {
    $("#addaform").hide();
    $("#cancelalias").hide();  
    $("#addalias").hide();
       $.ajax({
          data : {
             biib_code_alias : $("#biib_code_alias").val(),
             alias: $("#alias").val(),
             alias_type: $("#alias_type").val(),
                 },
             type : 'POST',
             url : '/addalias'

            })
        .done(function(data) {
          $("#output").text(data.output).show();
        alert(data.output);
        makealtable();  
      });
      event.preventDefault();
      };
      
     
function addmeasures() {
      $.ajax({
        data : {
             measure_id : $("#measure_select").val(),
             sample_id: $("#sample_select").val(),
             operation_type: $("#operation_select").val(),
                 },
             type : 'POST',
             url : '/addmeasure'

            })
        .done(function(data) {
       var measure_name = $("#measure_select").children("option:selected").text(); 
       alert(measure_name + " has been added");
      updatemeasures();
      });
      event.preventDefault();
      };
      
function addprocess() {
       $.ajax({
          data : {
             biib_code : $("#product_select").val(),
             mfg_mode : $("#mfg_mode").val(),
             process_identifier : $("#process_identifier").val(),
             process_description : $("#process_description").val(),
             process_status : $("#process_status").val(),
                 },
             type : 'POST',
             url : '/addprocess'
            })
        .done(function(data) {
         $("#processform").hide();
         $("#addprocess").hide();
         $("#cancelprocess").hide();
         alert(data.output); 
           getprocesses();    
      });
      event.preventDefault();
      };

function addoperation() {
       $.ajax({
          data : {
             biib_code : $("#product_select").val(),
             pro_id :  $("#process_select").val(),
             operation_order :  $("#operation_order").val(),
             operation_name :  $("#operation_name").val(),
             operation_type :  $("#operation_type").val(),
             chrom_type :  $("#chrom_type").val(),
             adjustment_type :  $("#adjustment_type").val(),
             adjustment_alias :  $("#adjustment_alias").val(),
             cycle_pool :  $("#cycle_pool").val(),
             bpr_order :  $("#bpr_order").val(),
                 },
             type : 'POST',
             url : '/addoperation'

            })
        .done(function(data) {
       alert(data.opalert);
        $("#newopform").hide();
        $("#addoperation").hide();
        $("#cancelop").hide();

        var ele = document.getElementById("operation_select"); 
        for (var i = 0; i < data.newop_choices.length; i++) {
            ele.innerHTML = ele.innerHTML +
 '<option value="' + data.newop_choices[i]['operation_id'] + "|" + data.newop_choices[i]['operation_type'] + '">' + data.newop_choices[i]['operation_order'] + "-" + data.newop_choices[i]['operation_full_name'] + '</option>'; };

      });
      event.preventDefault();
      };


function addsample() {
       $.ajax({
          data : {
             operation_type :  $("#operation_select").val(),
             sample_name :  $("#sample_name").val(),
             sample_order :  $("#sample_order").val(),
                 },
             type : 'POST',
             url : '/addsample'

            })
        .done(function(data) {
       alert(data.sampalert);
        $("#newsampform").hide();
        $("#addsample").hide();
        $("#cancelsamp").hide();

        var ele = document.getElementById("sample_select"); 
        for (var i = 0; i < data.newsamp_choices.length; i++) {
            ele.innerHTML = ele.innerHTML +
 '<option value="' + data.newsamp_choices[i]['sample_id'] + '">' + data.newsamp_choices[i]['sample_order'] + "-" + data.newsamp_choices[i]['sample_name'] + '</option>'; };
 
      });
      event.preventDefault();
      };

function addnewmeasure() {
       $.ajax({
          data : {
             measure_name :  $("#measure_name").val(),
             units_of_measure :  $("#units_of_measure").val()
                 },
             type : 'POST',
             url : '/addnewmeasure'

            })
        .done(function(data) {
       alert(data.measalert);
        $("#newmeasform").hide();
        $("#addnewmeas").hide();
        $("#cancelmeas").hide();
      });
      event.preventDefault();
      getmeasures();
      };
            
function makeprodtable(){
  var table = new Tabulator("#product_table", {
    layout:"fitData", //fit columns to width of table (optional)
    ajaxURL:"/getproducttable", //ajax URL
    ajaxParams:{ biib_code : $("#product_select").val() }, //ajax parameters
    ajaxConfig:"POST",
    ajaxResponse:function(url, params, response){
        return response.products; 
    },
    columns:[ //Define Table Columns
        {title:"Biib Code", field:"biib_code", width:100},
        {title:"Partner", field:"partner", width:125, editor:"select", editorParams:{"Biogen":"Biogen","Bioverativ":"Bioverativ","Janssen":"Janssen","Samsung":"Samsung","Celgene":"Celgene"}},
        {title:"Modality", field:"modality", width:125, editor:"select", editorParams:{"Protein":"Protein","Small Molecule":"Small Molecule","ASO":"ASO","Gene Therapy":"Gene Therapy"}},
        {formatter:"buttonCross", width:40, align:"center", cellClick:function(e, cell){
        if(confirm('Are you sure you want to delete this entry?'))
        var deldata = cell.getRow().getData();
        var biib_code = deldata.biib_code;

        $.ajax({
               data: { 
                      'biib_code': biib_code, 
                   },
               url: "/deleteproduct",
               type: "POST",
               })
            cell.getRow().delete();
            $('option[value="'+biib_code+'"]').remove();

             }
},   
    ],
    cellEdited:function(cell){
        var datax = cell.getRow().getData();
        var biib_code = datax.biib_code;
        var partner = datax.partner;
        var modality = datax.modality;
        var column_name = cell.getColumn().getDefinition().field;
        
       $.ajax({
               data: { 
                      'biib_code': biib_code,
                      'partner': partner,
                      'modality': modality,
                      'column_name': column_name,
            
                   },
               url: "/updateproduct",
               type: "POST",
               })
    },
   
   });
    
   table.setData();   
};

function makealtable(){
    var altable = new Tabulator("#alias_table", {
    layout:"fitData", //fit columns to width of table (optional)
    ajaxURL:"/getalias", //ajax URL
    ajaxParams:{ biib_code : $("#product_select").val() }, //ajax parameters
    ajaxConfig:"POST",
    ajaxResponse:function(url, params, response){
        return response.aliases; 
    },
    columns:[ //Define Table Columns
        {title:"Biib Code", field:"biib_code", width:100},
        {title:"Alias", field:"alias", width:125, editor:true},
        {title:"Alias Type", field:"alias_type", width:125, editor:"select", editorParams:{"Alternate Name":"Alternate Name","Molecule Name":"Molecule Name","Partner Name":"Partner Name","Smart Batch Code":"Smart Batch Code","Target Protein":"Target Protein","Trade Name":"Trade Name"}},
        {formatter:"buttonCross", width:40, align:"center", cellClick:function(e, cell){
        if(confirm('Are you sure you want to delete this entry?'))
        var deldata = cell.getRow().getData();
        var biib_code = deldata.biib_code;
        var alias = deldata.alias
        $.ajax({
               data: { 
                      'biib_code': biib_code,
                      'alias': alias 
                   },
               url: "/deletealias",
               type: "POST",
               })
                  
        cell.getRow().delete();
}},   
    ],
    cellEdited:function(cell){
        var datax = cell.getRow().getData();
        var biib_code = datax.biib_code;
        var alias = datax.alias;
        var alias_type = datax.alias_type;
        var column_name = cell.getColumn().getDefinition().field;
        
       $.ajax({
               data: { 
                      'biib_code': biib_code,
                      'alias': alias,
                      'alias_type': alias_type,
                      'column_name': column_name,
            
                   },
               url: "/updatealias",
               type: "POST",
               })
    },
   
   });
    
   altable.setData();
   
};
      
function makeproctable(){
 var protable = new Tabulator("#process_table", {
    layout:"fitData", //fit columns to width of table (optional)
    ajaxURL:"/getprocesstable", //ajax URL
    ajaxParams:{ pro_id : $("#process_select").val() }, //ajax parameters
    ajaxConfig:"POST",
    ajaxResponse:function(url, params, response){
        return response.processes; 
    },
    columns:[ //Define Table Columns
        {title:"ID", field:"process_id", width:40},
        {title:"MFG Mode", field:"mfg_mode", width:100, editor:"select", editorParams:{"DS":"DS","API":"API","SMDP":"SMDP","LMDP":"LMDP","ASO":"ASO"}},
        {title:"Process Identifier", field:"process_identifier", width:125, editor:true},
        {title:"Description", field:"process_description", width:150, editor:true},
        {title:"Status", field:"process_status", width:75, editor:"select", editorParams:{"ACTIVE":"ACTIVE","RETIRED":"RETIRED"}},
        {formatter:"buttonCross", width:40, align:"center", cellClick:function(e, cell){
        if(confirm('Are you sure you want to delete this entry?'))
        var deldata = cell.getRow().getData();
        var process_id = deldata.process_id;
        $.ajax({
               data: { 
                      'process_id': process_id,
                   },
               url: "/deleteprocess",
               type: "POST",
               })
        .done(function(data){
        getprocesses();
        cell.getRow().delete();
        })    
    
}},   
    ],
    cellEdited:function(cell){
        var datax = cell.getRow().getData();
        var process_id = datax.process_id;
        var mfg_mode = datax.mfg_mode;
        var process_identifier = datax.process_identifier;
        var process_description = datax.process_description;
        var process_status = datax.process_status;
        var column_name = cell.getColumn().getDefinition().field;
        
       $.ajax({
               data: { 
                      'process_id': process_id,
                      'mfg_mode': mfg_mode,
                      'process_identifier': process_identifier,
                      'process_description': process_description,
                      'process_status': process_status,
                      'column_name': column_name,
            
                   },
               url: "/updateprocess",
               type: "POST",
               })
       .done(function(data){
        getprocesses();
        });
     
    },
   
   });
    
   protable.setData();
   };
         
function updatemeasures() {
 var measuretable = new Tabulator("#measure_table", {
    layout:"fitData", //fit columns to width of table (optional)
    ajaxURL:"/getmeasuretable", //ajax URL
    ajaxParams:{ operation_type : $("#operation_select").val(),
                 sample_id: $("#sample_select").val(),
                 pro_id: $("#process_select").val(), }, //ajax parameters
    ajaxConfig:"POST",
    ajaxResponse:function(url, params, response){
        return response.measures; 
    },
    columns:[ //Define Table Columns
        {title:"ID", field:"parameter_id"},
        {title:"Measure Name", field:"measure_name", width:150},
        {title:"UoM", field:"units_of_measure", width:100},
        {title:"Mapping Replicates", field:"mapping_replicates", width:100, editor:true},
        {formatter:"buttonCross", width:40, align:"center", cellClick:function(e, cell){
        if(confirm('Are you sure you want to delete this entry?'))
        var deldata = cell.getRow().getData();
        var parameter_id = deldata.parameter_id;
        $.ajax({
               data: { 
                      'parameter_id': parameter_id,
                   },
               url: "/deletemeasure",
               type: "POST",
               })
                  
        cell.getRow().delete();
}},   
    ],
        cellEdited:function(cell){
        var datax = cell.getRow().getData();
        var parameter_id = datax.parameter_id;
        var mapping_replicates = datax.mapping_replicates;
         console.log(mapping_replicates);                

       $.ajax({
               data: { 
                      'parameter_id': parameter_id,
                      'mapping_replicates': mapping_replicates, 
            
                   },
               url: "/updatemapreplicates",
               type: "POST",
               })
            .done(function(data){
                      console.log(mapping_replicates);                
                    })
                       }
       
   });    
   measuretable.setData();
   };
         
function gotoprimr() {
     $("#opspage").hide();
     $("#primrpage").show();
     
    var primrtable = new Tabulator("#primr_table", {
    height: 600, // set height of table (in CSS or here), this enables the Virtual DOM and improves render speed dramatically (can be any valid css height value)
    layout:"fitDataFill", //fit columns to width of table (optional)
    addRowPos:"bottom",
    ajaxURL:"/getprimr", //ajax URL
    ajaxParams:{pro_id: $("#process_select").val() }, //ajax parameters
    ajaxConfig:"POST",
    ajaxResponse:function(url, params, response){
        return response.primrs; 
    },
    columns:[ //Define Table Columns
        {title:"Parameter ID", field:"parameter_id", width:50 },
        {title:"Mapping ID", field:"mapping_id", width:50 },
        {title:"Operation Name", field:"operation_name", width:200, headerFilter:true},
        {title:"Operation Type", field:"operation_type", width:200, headerFilter:true},
        {title:"Adjustment Type", field:"adjustment_type", width:150, headerFilter:true},
        {title:"Sample Name", field:"sample_name", width:150, headerFilter:true},
        {title:"Measure Name", field:"measure_name", width:150, headerFilter:true},
        {title:"UoM", field:"units_of_measure", width:80},   
        {title:"PRIMR Book Template Name", field:"primr_book_template_name", width:250, editor:true},
        {title:"PRIMR Page Name", field:"primr_page_name", width:200, editor:true},
        {title:"PRIMR Field Name", field:"primr_field_name", width:150, editor:true},
        {title:"Row Number", field:"primr_row_number", width:100, editor:true},
        {title:"Iteration", field:"parameter_iteration", width:100, editor:true},   
        {formatter:"buttonCross", width:40, align:"center", cellClick:function(e, cell){
        if(confirm('Are you sure you want to delete this entry?'))
        var deldata = cell.getRow().getData();
        var mapping_id = deldata.mapping_id;
        $.ajax({
               data: { 
                      'mapping_id': mapping_id,
                   },
               url: "/deleteprimr",
               type: "POST",
               })
                  
        cell.getRow().delete();
}}, 
    ],
    cellEdited:function(cell){
        var datax = cell.getRow().getData();
        var row_id = datax.id;
        var parameter_id = datax.parameter_id;
        var mapping_id = datax.mapping_id;
        var parameter_iteration = datax.parameter_iteration;
        var primr_book_template_name = datax.primr_book_template_name;
        var primr_page_name = datax.primr_page_name;
        var primr_field_name = datax.primr_field_name;
        var primr_row_number = datax.primr_row_number;
        var column_name = cell.getColumn().getDefinition().field;
        if (!mapping_id){
        $("#primr_table").hide();
        $("#newprimrtext").show();}
       $.ajax({
               data: { 
                      'parameter_id': parameter_id,
                      'mapping_id': mapping_id, 
                      'parameter_iteration': parameter_iteration,
                      'primr_book_template_name': primr_book_template_name,
                      'primr_page_name': primr_page_name,
                      'primr_field_name': primr_field_name,
                      'primr_row_number': primr_row_number,
                      'column_name': column_name,
            
                   },
               url: "/updateprimr",
               type: "POST",
               })
               .done(function(data) {
            if (!mapping_id){
            $("#primr_table").show();
            $("#newprimrtext").hide();
            primrtable.setData("/getprimr", {pro_id: $("#process_select").val() });
          }
})
        }
        });
   primrtable.setData();  
};


function gotoparams() {
     $("#opspage").hide();
     $("#productpage").hide();
     $("#paramspage").show();
var table = new Tabulator("#parameters_table", {
    height: 600, // set height of table (in CSS or here), this enables the Virtual DOM and improves render speed dramatically (can be any valid css height value)
    layout:"fitDataFill", //fit columns to width of table (optional)
    ajaxURL:"/getparam", //ajax URL
    ajaxParams:{pro_id: $("#process_select").val() }, //ajax parameters
    ajaxConfig:"POST",
    ajaxResponse:function(url, params, response){
        return response.parameters; 
    },
    columns:[ //Define Table Columns
        {title:"Operation Name", field:"operation_name", width:200, headerFilter:true, freeze:true},
        {title:"Operation Type", field:"operation_type", width:200, headerFilter:true, freeze:true},
        {title:"Adjustment Type", field:"adjustment_type", width:150, headerFilter:true, freeze:true},
        {title:"Sample Name", field:"sample_name", width:150, headerFilter:true, freeze:true},
        {title:"Measure Name", field:"measure_name", width:150, headerFilter:true, freeze:true},
        {title:"UoM", field:"units_of_measure", width:100},
        {title:"Parameter Name", field:"parameter_name", editor:true},
        {title:"Decimal Display", field:"decimal_display", editor:true},
        {title:"Target", field:"target", editor:true},
        {title:"LOR", field:"lor", editor:true},
        {title:"UOR", field:"uor", editor:true},
        {title:"LAL", field:"lal", editor:true},
        {title:"UAL", field:"ual", editor:true},
        {title:"LSL", field:"lsl", editor:true},
        {title:"USL", field:"usl", editor:true},
        {title:"LCL", field:"lcl", editor:true},
        {title:"UCL", field:"ucl", editor:true},
        {title:"Class", field:"classification", editor:true, headerFilter:true},
        {formatter:"buttonCross", width:40, align:"center", cellClick:function(e, cell){
        if(confirm('Are you sure you want to delete this entry?'))
        var deldata = cell.getRow().getData();
        var param_id = deldata.id;
        $.ajax({
               data: { 
                      'param_id': param_id, 
                   },
               url: "/deleteparam",
               type: "POST",
               })
                  
        cell.getRow().delete();
}},
    ],
    
   cellEdited:function(cell){
        var datax = cell.getRow().getData();
        var param_id = datax.id;
        var parameter_name = datax.parameter_name;
        var decimal_display = datax.decimal_display;
        var target = datax.target;
        var lor = datax.lor;
        var uor = datax.uor;
        var lal = datax.lal;
        var ual = datax.ual;
        var lsl = datax.lsl;
        var usl = datax.usl;
        var lcl = datax.lcl;
        var ucl = datax.ucl;
        var classification = datax.classification;
        var column_name = cell.getColumn().getDefinition().field;
        
       $.ajax({
               data: { 
                      'param_id': param_id,
                      'parameter_name':parameter_name,
                      'decimal_display':decimal_display,
                      'target': target,
                      'lor': lor,
                      'uor': uor,
                      'lal': lal,
                      'ual': ual,
                      'lsl': lsl,
                      'usl': usl,
                      'lcl': lcl,
                      'ucl': ucl,
                      'classification': classification,
                      'column_name': column_name,
            
                   },
               url: "/updateparam",
               type: "POST",
               dataType: "json",

               })
    },
    
    });

   table.setData();
};

function gotodeltav() {
     $("#opspage").hide();
     $("#deltavpage").show();
     
    var deltavtable = new Tabulator("#deltav_table", {
    height: 600, // set height of table (in CSS or here), this enables the Virtual DOM and improves render speed dramatically (can be any valid css height value)
    layout:"fitDataFill", //fit columns to width of table (optional)
    addRowPos:"bottom",
    ajaxURL:"/getdeltav", //ajax URL
    ajaxParams:{pro_id: $("#process_select").val() }, //ajax parameters
    ajaxConfig:"POST",
    ajaxResponse:function(url, params, response){
        return response.deltavs; 
    },
    columns:[ //Define Table Columns
        {title:"Parameter ID", field:"parameter_id", width:50 },
        {title:"Mapping ID", field:"mapping_id", width:50 },
        {title:"Operation Name", field:"operation_name", width:200, headerFilter:true},
        {title:"Operation Type", field:"operation_type", width:200, headerFilter:true},
        {title:"Adjustment Type", field:"adjustment_type", width:150, headerFilter:true},
        {title:"Sample Name", field:"sample_name", width:150, headerFilter:true},
        {title:"Measure Name", field:"measure_name", width:150, headerFilter:true},
        {title:"UoM", field:"units_of_measure", width:80},   
        {title:"Unit", field:"dv_unit", width:100, editor:true},
        {title:"Process Phase", field:"dv_process_phase", width:100, editor:true},
        {title:"Category", field:"dv_category", width:100, editor:true},
        {title:"Cycle", field:"dv_cycle", width:100, editor:true},
        {title:"XLT Process Name", field:"dv_xlt_process_name", width:100, editor:true},
        {title:"Unit Procedure", field:"dv_unit_procedure", width:100, editor:true},
        {title:"Iteration", field:"dv_iteration", width:100, editor:true},
        {title:"Output Col", field:"dv_output_col", width:100, editor:"select", editorParams:{"NUMBER_VALUE":"NUMBER_VALUE"}},
        {formatter:"buttonCross", width:40, align:"center", cellClick:function(e, cell){ 
        if(confirm('Are you sure you want to delete this entry?'))
        var deldata = cell.getRow().getData();
        var mapping_id = deldata.mapping_id;
        $.ajax({
               data: { 
                      'mapping_id': mapping_id,
                   },
               url: "/deletedeltav",
               type: "POST",
               })
                  
        cell.getRow().delete();
}}, 
    ],

    cellEdited:function(cell){
        var datax = cell.getRow().getData();
        var parameter_id = datax.parameter_id;
        var mapping_id = datax.mapping_id;
        var dv_unit = datax.dv_unit;
        var dv_process_phase = datax.dv_process_phase;
        var dv_category = datax.dv_category;
        var dv_cycle = datax.dv_cycle;
        var dv_xlt_process_name = datax.dv_xlt_process_name;
        var dv_unit_procedure = datax.dv_unit_procedure;
        var dv_iteration = datax.dv_iteration;
        var dv_output_col = datax.dv_output_col;
        var column_name = cell.getColumn().getDefinition().field;
        if (!mapping_id){
        $("#deltav_table").hide();
        $("#newdeltavtext").show();}
 
       $.ajax({
               data: { 
                      'parameter_id': parameter_id,
                      'mapping_id': mapping_id, 
                      'dv_unit': dv_unit,
                      'dv_process_phase': dv_process_phase,
                      'dv_category': dv_category,
                      'dv_cycle': dv_cycle,
                      'dv_xlt_process_name': dv_xlt_process_name,
                      'dv_unit_procedure': dv_unit_procedure,
                      'dv_iteration': dv_iteration,
                      'dv_output_col': dv_output_col,
                      'column_name': column_name,
                   },
               url: "/updatedeltav",
               type: "POST",
               })
      .done(function(data) {
            if (!mapping_id){
            $("#deltav_table").show();
            $("#newdeltavtext").hide();
            deltavtable.setData("/getdeltav", {pro_id: $("#process_select").val() });
          }
})
    },
   
   });    
   deltavtable.setData();
};

function gotoeditops() {
     $("#opspage").hide();
     $("#editopspage").show();
     
    editopstable = new Tabulator("#editops_table", {
    layout:"fitDataFill", //fit columns to width of table (optional)
    movableRows:true,
    ajaxURL:"/geteditops", //ajax URL
    ajaxParams:{pro_id: $("#process_select").val() }, //ajax parameters
    ajaxConfig:"POST",
    ajaxResponse:function(url, params, response){
        return response.editops; 
    },
    columns:[ //Define Table Columns
        {rowHandle:true, formatter:"handle", headerSort:false, frozen:true, width:30, minWidth:30},
        {title:"ID", field:"operation_id", width:50 },
        {title:"Operation Order", field:"operation_order", width:100, editor:true},
        {title:"Operation Name", field:"operation_name", width:200, editor:true},
        {title:"Operation Type", field:"operation_type", width:200, editor:true},
        {title:"Chrom Type", field:"chrom_type", width:125, editor:"select", editorParams:{"NULL":"N/A","BINDELUTE":"BINDELUTE","FLOWTHROUGH":"FLOWTHROUGH"}},
        {title:"Adjustment Type", field:"adjustment_type", width:150, editor:"select", editorParams:{"NULL":"N/A","pH Up":"pH Up","pH Down":"pH Down","pH Up/Down":"pH Up/Down","Conductivity Up":"Conductivity Up", "Conductivity Up/Down":"Conductivity Up/Down","Surfactant":"Surfactant","Formulation":"Formulation"}},   
        {title:"Adjustment Alias", field:"adjustment_alias", width:150, editor:true},
        {title:"Cycle/Pool", field:"cycle_pool", width:150, editor:"select", editorParams:{"NULL":"N/A","PER_CYCLE":"PER_CYCLE","POOL":"POOL"}},
        {title:"BPR Order", field:"bpr_order", width:125, editor:true},
        {formatter:"buttonCross", width:40, align:"center", cellClick:function(e, cell){
        if(confirm('Are you sure you want to delete this entry?'))
        var deldata = cell.getRow().getData();
        var operation_id = deldata.operation_id;
        $.ajax({
               data: { 
                      'operation_id': operation_id, 
                   },
               url: "/deleteeditops",
               type: "POST",
               })
                  
        cell.getRow().delete();
}},
    ],
    
    
     cellEdited:function(cell){
        var datax = cell.getRow().getData();
        var operation_id = datax.operation_id;
        var operation_order = datax.operation_order;
        var operation_type = datax.operation_type;
        var operation_name = datax.operation_name;
        var chrom_type = datax.chrom_type;
        var adjustment_type = datax.adjustment_type;
        var adjustment_alias = datax.adjustment_alias;
        var cycle_pool = datax.cycle_pool;
        var bpr_order = datax.bpr_order;
        var column_name = cell.getColumn().getDefinition().field;
        
       $.ajax({
               data: { 
                      'operation_id': operation_id, 
                      'operation_order': operation_order,
                      'operation_type': operation_type,
                      'operation_name': operation_name,
                      'chrom_type': chrom_type,
                      'adjustment_type': adjustment_type,
                      'adjustment_alias': adjustment_alias,
                      'cycle_pool': cycle_pool,
                      'bpr_order': bpr_order,
                      'column_name': column_name,
            
                   },
               url: "/updateeditops",
               type: "POST",
               dataType: "json",

               })
      
    },
    
    
    });

   editopstable.setData();
};

function gotoeditsamps() {
     $("#opspage").hide();
     $("#editsampspage").show();
    editsampstable = new Tabulator("#editsamps_table", {
    layout:"fitDataFill", //fit columns to width of table (optional)
    movableRows: true,
    ajaxURL:"/geteditsamps", //ajax URL
    ajaxParams:{operation_type: $("#operation_select").val() }, //ajax parameters
    ajaxConfig:"POST",
    ajaxResponse:function(url, params, response){
        return response.editsamps; 
    },
    columns:[ //Define Table Columns
        {rowHandle:true, formatter:"handle", headerSort:false, frozen:true, width:30, minWidth:30},
        {title:"ID", field:"sample_id", width:90 },
        {title:"Sample Order", field:"sample_order", width:150, editor:true, sorter:"number"},
        {title:"Sample Name", field:"sample_name", width:375, editor:true},
        {formatter:"buttonCross", width:40, align:"center", cellClick:function(e, cell){
        if(confirm('Are you sure you want to delete this entry?'))
        var deldata = cell.getRow().getData();
        var sample_id = deldata.sample_id;
        $.ajax({
               data: { 
                      'sample_id': sample_id, 
                   },
               url: "/deleteeditsamps",
               type: "POST",
               })
                  
        cell.getRow().delete();
}},
    ],  
    
    rowMoved:function(row){
        console.log("Row: " + row.getData().name + " has been moved");
    },

   cellEdited:function(cell){
        var datax = cell.getRow().getData();
        var sample_id = datax.sample_id;
        var sample_order = datax.sample_order;
        var sample_name = datax.sample_name;
        var column_name = cell.getColumn().getDefinition().field;
        
       $.ajax({
               data: { 
                      'sample_id': sample_id, 
                      'sample_order': sample_order,
                      'sample_name': sample_name,
                      'column_name': column_name,
                   },
               url: "/updateeditsamps",
               type: "POST",
               dataType: "json",

               })
    },
    
    });

   editsampstable.setData();
};

function gotoeditmeas() {
     $("#opspage").hide();
     $("#editmeaspage").show();
var allmeastable = new Tabulator("#allmeas_table", {
    layout:"fitDataFill", //fit columns to width of table (optional)
    height: 700, // set height of table (in CSS or here), this enables the Virtual DOM and improves render speed dramatically (can be any valid css height value)
    ajaxURL:"/geteditallmeas", //ajax URL
    ajaxConfig:"POST",
    ajaxResponse:function(url, params, response){
        return response.editallmeas; 
    },
    columns:[ //Define Table Columns
        {title:"ID", field:"measure_id", width:50 },
        {title:"Measure Name", field:"measure_name", width:300},
        {title:"Units of Measure", field:"units_of_measure", width:200, editor:true},
        {title:"Data Type", field:"data_type", width:200, editor:"select", editorParams:{"NULL":"N/A","string":"string","numeric":"numeric","DateTime":"DateTime"}},
        {formatter:"buttonCross", width:40, align:"center", cellClick:function(e, cell){
        if(confirm('Are you sure you want to delete this entry?'))
        var deldata = cell.getRow().getData();
        var measure_id = deldata.measure_id;
        $.ajax({
               data: { 
                      'measure_id': measure_id, 
                   },
               url: "/deleteeditmeas",
               type: "POST",
               })
                  
        cell.getRow().delete();
}},
    ],
    
   cellEdited:function(cell){
        var datax = cell.getRow().getData();
        var measure_id = datax.measure_id;
        var measure_name = datax.measure_name;
        var units_of_measure = datax.units_of_measure;
        var data_type = datax.data_type;
        var column_name = cell.getColumn().getDefinition().field;
        
       $.ajax({
               data: { 
                      'measure_id': measure_id, 
                      'measure_name': measure_name,
                      'units_of_measure': units_of_measure,
                      'data_type': data_type,
                      'column_name': column_name,
                   },
               url: "/updateeditmeas",
               type: "POST",
               dataType: "json",

               })
    },
    
    });

   allmeastable.setData();
};

function gotoles() {
     $("#opspage").hide();
     $("#lespage").show();
     
    var lestable = new Tabulator("#les_table", {
    height: 600, // set height of table (in CSS or here), this enables the Virtual DOM and improves render speed dramatically (can be any valid css height value)
    layout:"fitDataFill", //fit columns to width of table (optional)
    ajaxURL:"/getles", //ajax URL
    ajaxParams:{pro_id: $("#process_select").val() }, //ajax parameters
    ajaxConfig:"POST",
    ajaxResponse:function(url, params, response){
        return response.les; 
    },
    columns:[ //Define Table Columns
        {title:"Parameter ID", field:"parameter_id", width:50 },
        {title:"Mapping ID", field:"mapping_id", width:50 },
        {title:"Operation Name", field:"operation_name", width:150, headerFilter:true},
        {title:"Operation Type", field:"operation_type", width:150, headerFilter:true},
        {title:"Adjustment Type", field:"adjustment_type", width:150, headerFilter:true},
        {title:"Sample Name", field:"sample_name", width:150, headerFilter:true},
        {title:"Measure Name", field:"measure_name", width:150, headerFilter:true},
        {title:"UoM", field:"units_of_measure", width:50},   
        {title:"Sample Product", field:"sample_product", width:100, editor:true},
        {title:"Sample Stage", field:"sample_stage", width:100, editor:true},
        {title:"Test Analysis", field:"test_analysis", width:100, editor:true},
        {title:"Result Name", field:"result_name", width:100, editor:true},
        {title:"Parameter Iteration", field:"parameter_iteration", width:50, editor:true},
        {formatter:"buttonCross", width:40, align:"center", cellClick:function(e, cell){ 
        if(confirm('Are you sure you want to delete this entry?'))
        var deldata = cell.getRow().getData();
        var mapping_id = deldata.mapping_id;
        $.ajax({
               data: { 
                      'mapping_id': mapping_id,
                   },
               url: "/deleteles",
               type: "POST",
               })
                  
        cell.getRow().delete();
}}, 
    ],
    cellEdited:function(cell){
        var datax = cell.getRow().getData();
        var parameter_id = datax.parameter_id;
        var parameter_iteration = datax.parameter_iteration;
        var mapping_id = datax.mapping_id;
        var sample_product = datax.sample_product;
        var sample_stage = datax.sample_stage;
        var test_analysis = datax.test_analysis;
        var result_name = datax.result_name;
        var column_name = cell.getColumn().getDefinition().field;
        if (!mapping_id){
        $("#les_table").hide();
        $("#newlestext").show();}
 
       $.ajax({
               data: { 
                      'parameter_id': parameter_id,
                      'mapping_id': mapping_id, 
                      'parameter_iteration': parameter_iteration,
                      'sample_product': sample_product,
                      'sample_stage': sample_stage,
                      'test_analysis': test_analysis,
                      'result_name': result_name,
                      'column_name': column_name,
     
                   },
               url: "/updateles",
               type: "POST",
               })
               
      .done(function(data) {
            if (!mapping_id){
            $("#les_table").show();
            $("#newlestext").hide();
            lestable.setData("/getles", {pro_id: $("#process_select").val() });
          }
})
    },
   
   });    
   lestable.setData();
};

function editopsrow() {   
    var order_data = editopstable.getData();
    var loop_length = order_data.length;
    var op_order_list = [];
    for (i = 0; i < order_data.length; i++) {
        var new_row = i;
        var operation_id = order_data[i].operation_id;
        var old_row = order_data[i].operation_order;
        var op_order = operation_id + "-" + old_row + "/" + new_row; 
        op_order_list.push(op_order);
    };
       alert("Reordering Operations..");

    $.ajax({
    data: { 
            'op_order_list':op_order_list, 
            },
          url: "/editopsrow",
               type: "GET",
               })
      .done(function(data) {
      console.log(op_order_list)
      editopstable.setData();
    });
    };
        
function editsampsrow() {   
    var samp_data = editsampstable.getData();
    var loop_length = samp_data.length;
    var samp_order_list = [];
    for (i = 0; i < samp_data.length; i++) {
        var new_row = i;
        var sample_id = samp_data[i].sample_id;
        var old_row = samp_data[i].sample_order;
        var samp_order = sample_id + "-" + old_row + "/" + new_row; 
        samp_order_list.push(samp_order);
    };
       alert("Reordering Samples..");

    $.ajax({
    data: { 
            'samp_order_list':samp_order_list, 
            },
          url: "/editsampsrow",
               type: "GET",
               })
      .done(function(data) {
      console.log(samp_order_list)
      editsampstable.setData();
    });
    };    
    
function gotodvpi() {
     $("#opspage").hide();
     $("#dvpipage").show();
     
    var dvpitable = new Tabulator("#dvpi_table", {
    height: 600, // set height of table (in CSS or here), this enables the Virtual DOM and improves render speed dramatically (can be any valid css height value)
    layout:"fitDataFill", //fit columns to width of table (optional)
    ajaxURL:"/getdvpi", //ajax URL
    ajaxParams:{pro_id: $("#process_select").val() }, //ajax parameters
    ajaxConfig:"POST",
    ajaxResponse:function(url, params, response){
        return response.dvpis; 
    },
    columns:[ //Define Table Columns
        {title:"Parameter ID", field:"parameter_id", width:50 },
        {title:"Mapping ID", field:"mapping_id", width:50 },
        {title:"Operation Name", field:"operation_name", width:200, headerFilter:true},
        {title:"Operation Type", field:"operation_type", width:200, headerFilter:true},
        {title:"Adjustment Type", field:"adjustment_type", width:150, headerFilter:true},
        {title:"Sample Name", field:"sample_name", width:150, headerFilter:true},
        {title:"Measure Name", field:"measure_name", width:150, headerFilter:true},
        {title:"UoM", field:"units_of_measure", width:80},   
        {title:"Unit Procedure", field:"unitprocedure", width:250, editor:true},
        {title:"Operation", field:"operation", width:250, editor:true},
        {title:"Phase", field:"phase", width:250, editor:true},
        {title:"Attribute", field:"attribute", width:250, editor:true},
        {title:"Iteration", field:"parameter_iteration", width:250, editor:true},   
        {formatter:"buttonCross", width:40, align:"center", cellClick:function(e, cell){
        if(confirm('Are you sure you want to delete this entry?'))
        var deldata = cell.getRow().getData();
        var mapping_id = deldata.mapping_id;
        $.ajax({
               data: { 
                      'mapping_id': mapping_id,
                   },
               url: "/deletedvpi",
               type: "POST",
               })
                  
        cell.getRow().delete();
}}, 
    ],
    cellEdited:function(cell){
        var datax = cell.getRow().getData();
        var row_id = datax.id;
        var parameter_id = datax.parameter_id;
        var mapping_id = datax.mapping_id;
        var parameter_iteration = datax.parameter_iteration;
        var unitprocedure = datax.unitprocedure;
        var operation = datax.operation;
        var phase = datax.phase;
        var attribute = datax.attribute;
        var column_name = cell.getColumn().getDefinition().field;
        if (!mapping_id){
        $("#dvpi_table").hide();
        $("#newdvpitext").show();}
       $.ajax({
               data: { 
                      'parameter_id': parameter_id,
                      'mapping_id': mapping_id, 
                      'parameter_iteration': parameter_iteration,
                      'unitprocedure': unitprocedure,
                      'operation': operation,
                      'phase': phase,
                      'attribute': attribute,
                      'column_name': column_name,
            
                   },
               url: "/updatedvpi",
               type: "POST",
               })
       .done(function(data) {
            if (!mapping_id){
            $("#dvpi_table").show();
            $("#newdvpitext").hide();
            dvpitable.setData("/getdvpi", {pro_id: $("#process_select").val() });
          }
})
        },
        });
   dvpitable.setData();  
};