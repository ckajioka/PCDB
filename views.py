#These imports are just connecting the different python files in app
from application import app
from db import db
from models import PCDB_PRODUCT, PCDB_PRODUCT_PROCESS, PCDB_PRODUCT_ALIAS, PCDB_OPERATION, PCDB_SAMPLE, PCDB_MEASURE, PCDB_PARAMETERS, PCDB_PRIMR, PCDB_PARAMETERS_V, PCDB_PRIMR_V, PCDB_DELTAV, PCDB_DELTAV_V, PCDB_AUDIT_TRAIL, PCDB_LES, PCDB_LES_V, PCDB_DVPI, PCDB_DVPI_V
from forms import pcdb_new_product_alias_form, pcdb_new_product_process_form, pcdb_new_product_form, pcdb_new_operation_form, pcdb_new_sample_form, pcdb_new_measure_form

#Other packages something
from flask import render_template, jsonify, request
import datetime

@app.route('/', methods = ['GET', 'POST'])
def index():
    #imports product select form from forms file and renders 
    aliasform =  pcdb_new_product_alias_form()
    addprocform = pcdb_new_product_process_form()
    addproform = pcdb_new_product_form()
    newopform = pcdb_new_operation_form()
    newsampform = pcdb_new_sample_form()
    newmeasform = pcdb_new_measure_form()
    #Need to include variables to be used in jinja2 templates
    return render_template('home.html', newsampform=newsampform,newopform=newopform, aliasform=aliasform, addprocform=addprocform, addproform=addproform, newmeasform=newmeasform)    

@app.route('/getbiibs', methods=['GET','POST'])
def getbiibs():
    #Queries database and returns data based upon the biib code selected
    results = PCDB_PRODUCT.query.with_entities(PCDB_PRODUCT.biib_code).all()
   #Results are stored as a list of tuples - need to go through list and then unpack each tuple to get other data
    biib_choices = []
    for result in results:
        biib_code = result[0]
        product = {
                "biib_code": biib_code
                }
        biib_choices.append(product)
    return jsonify({'biib_choices': biib_choices})

@app.route('/getprocess', methods=['GET','POST'])
def getprocess():
    #Get the biib_code from the form
    biib_code = request.form.get('biib_code')
    #Need a blank list to add results into
    pro_choices = []
    #Queries database and returns data based upon the biib code selected
    results = PCDB_PRODUCT_PROCESS.query.with_entities(PCDB_PRODUCT_PROCESS.id, PCDB_PRODUCT_PROCESS.biib_code, PCDB_PRODUCT_PROCESS.mfg_mode, PCDB_PRODUCT_PROCESS.process_identifier, PCDB_PRODUCT_PROCESS.process_status).filter(PCDB_PRODUCT_PROCESS.biib_code==biib_code).all()
   #Results are stored as a list of tuples - need to go through list and then unpack each tuple to get other data
    for result in results:
        pro_id = result[0]
        biib_code = result[1]
        mfg_mode = result[2]
        #This part is just to stop errors - if a result doesn't exist it just skips it rather than freak out
        if not mfg_mode:
            mfg_mode = " "
        process_identifier = result[3]
        if not process_identifier:
            process_identifier = " "
        process_status = result[4]
        if not process_status:
            process_status = " "
        #This is needed to jsonify data - give each data point a title so page can interpret data   
        process = {
                "id": pro_id,
                "biib_code": biib_code,
                "mfg_mode": mfg_mode,
                "process_identifier": process_identifier,
                "process_status": process_status}
        pro_choices.append(process)
    return jsonify({'pro_choices': pro_choices})

@app.route('/getproducttable', methods=['GET','POST'])
def getproducttable():
    biib_code = request.form.get('biib_code') 
    products = []
    results = PCDB_PRODUCT.query.with_entities(PCDB_PRODUCT.biib_code, PCDB_PRODUCT.partner, PCDB_PRODUCT.modality).filter(PCDB_PRODUCT.biib_code==biib_code).all()
    for result in results:
        biib_code = result[0]
        partner = result[1]
        modality = result[2]

        #This is needed to jsonify data - give each data point a title so page can interpret data   
        product = {
        "biib_code": biib_code, 
        "partner": partner, 
        "modality": modality, 
            }
        products.append(product)
    return jsonify({'products':products})
    
@app.route('/updateproduct', methods=['GET','POST'])
def updateproduct():
    biib_code = request.form.get('biib_code')
    partner = request.form.get('partner')
    modality = request.form.get('modality')
    column_name = request.form.get('column_name')
    product = PCDB_PRODUCT.query.filter(PCDB_PRODUCT.biib_code==biib_code).first()
    oldpartner = product.partner
    oldmodality = product.modality
    if column_name == "biib_code":
        product.biib_code = biib_code
    if column_name == "partner":
        product.partner = partner
        updated_value = partner
        original_value = oldpartner
    if column_name == "modality":
        product.modality = modality
        updated_value = modality
        original_value = oldmodality
    db.session.commit()
    date_time = datetime.datetime.utcnow()
    change_type = "EDIT"
    table_name = "PCDB_PRODUCT"
    row_id = biib_code
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    db.session.commit()
    return ('', 204)

@app.route('/deleteproduct', methods=['GET','POST'])
def deleteproduct():
    biib_code = request.form.get('biib_code')
    product = PCDB_PRODUCT.query.filter(PCDB_PRODUCT.biib_code==biib_code).first()
    partner = product.partner
    modality = product.modality
    db.session.delete(product)
    db.session.commit()
    date_time = datetime.datetime.utcnow()
    table_name = "PCDB_PRODUCT"
    change_type = "DELETE"
    row_id = biib_code
    updated_value = ""
    column_name = "biib_code"
    original_value = biib_code
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    column_name = "partner"
    original_value = partner
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    column_name = "modality"
    original_value = modality
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    db.session.commit()
    return ('', 204)

@app.route('/getalias', methods=['GET','POST'])
def getalias():
    biib_code = request.form.get('biib_code') 
    aliases = []
    results = PCDB_PRODUCT_ALIAS.query.with_entities(PCDB_PRODUCT_ALIAS.biib_code, PCDB_PRODUCT_ALIAS.alias, PCDB_PRODUCT_ALIAS.alias_type).filter(PCDB_PRODUCT_ALIAS.biib_code==biib_code).all()
    for result in results:
        biib_code = result[0]
        alias = result[1]
        alias_type = result[2]
        #This is needed to jsonify data - give each data point a title so page can interpret data   
        alias = {
        "biib_code": biib_code, 
        "alias": alias, 
        "alias_type": alias_type, 
            }
        aliases.append(alias)
    return jsonify({'aliases':aliases})
    
@app.route('/updatealias', methods=['GET','POST'])
def updatealias():
    biib_code = request.form.get('biib_code')
    alias = request.form.get('alias')
    alias_type = request.form.get('alias_type')
    column_name = request.form.get('column_name')
    product = PCDB_PRODUCT_ALIAS.query.filter(PCDB_PRODUCT_ALIAS.biib_code==biib_code).first()
    oldalias = product.alias
    oldalias_type = product.alias_type
    row_id = product.id
    if column_name == "biib_code":
        product.biib_code = biib_code
    if column_name == "alias":
        product.alias = alias
        updated_value = alias
        original_value = oldalias
    if column_name == "alias_type":
        product.alias_type = alias_type
        updated_value = alias_type
        original_value = oldalias_type
    db.session.commit()
    date_time = datetime.datetime.utcnow()
    change_type = "EDIT"
    table_name = "PCDB_PRODUCT_ALIAS"
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    db.session.commit()
    return ('', 204)

@app.route('/deletealias', methods=['GET','POST'])
def deletealias():
    biib_code = request.form.get('biib_code')
    alias = request.form.get('alias')
    delalias = PCDB_PRODUCT_ALIAS.query.filter(PCDB_PRODUCT_ALIAS.biib_code==biib_code, PCDB_PRODUCT_ALIAS.alias==alias).first()
    row_id = delalias.id
    alias_type = delalias.alias_type
    db.session.delete(delalias)
    db.session.commit()
    date_time = datetime.datetime.utcnow()
    table_name = "PCDB_PRODUCT_ALIAS"
    change_type = "DELETE"
    updated_value = ""
    column_name = "biib_code"
    original_value = biib_code
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    column_name = "alias"
    original_value = alias
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    column_name = "alias_type"
    original_value = alias_type
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    db.session.commit()
    return ('', 204)

@app.route('/getprocesstable', methods=['GET','POST'])
def getprocesstable():
    pro_id = request.form.get('pro_id') 
    process_id = pro_id.split(')')[0]  
    processes = []
    results = PCDB_PRODUCT_PROCESS.query.with_entities(PCDB_PRODUCT_PROCESS.mfg_mode, PCDB_PRODUCT_PROCESS.process_identifier, PCDB_PRODUCT_PROCESS.process_description, PCDB_PRODUCT_PROCESS.process_status).filter(PCDB_PRODUCT_PROCESS.id==process_id).all()
    for result in results:
        mfg_mode = result[0]
        process_identifier = result[1]
        if process_identifier is None:
            process_identifier = ""
        process_description = result[2]
        process_status = result[3]
        #This is needed to jsonify data - give each data point a title so page can interpret data   
        process = {
        "process_id": process_id,        
        "mfg_mode": mfg_mode, 
        "process_identifier": process_identifier, 
        "process_description": process_description,
        "process_status": process_status
            }
        processes.append(process)
    return jsonify({'processes':processes})
    
@app.route('/updateprocess', methods=['GET','POST'])
def updateprocess():
    process_id = request.form.get('process_id')
    mfg_mode = request.form.get('mfg_mode')
    process_identifier = request.form.get('process_identifier')
    process_description = request.form.get('process_description')
    process_status = request.form.get('process_status')
    column_name = request.form.get('column_name')
    process = PCDB_PRODUCT_PROCESS.query.filter(PCDB_PRODUCT_PROCESS.id==process_id).first()
    oldmfg_mode = process.mfg_mode
    oldprocess_identifier = process.process_identifier
    oldprocess_description = process.process_description
    oldprocess_status = process.process_status
    if column_name == "mfg_mode":
        process.mfg_mode = mfg_mode
        updated_value = mfg_mode
        original_value = oldmfg_mode    
    if column_name == "process_identifier":
        process.process_identifier = process_identifier
        updated_value = process_identifier
        original_value = oldprocess_identifier
    if column_name == "process_description":
        process.process_description = process_description
        updated_value = process_description
        original_value = oldprocess_description
    if column_name == "process_status":
        process.process_status = process_status
        updated_value = process_status
        original_value = oldprocess_status
    db.session.commit()
    row_id = process_id
    date_time = datetime.datetime.utcnow()
    change_type = "EDIT"
    table_name = "PCDB_PRODUCT_PROCESS"
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    db.session.commit()
    return ('', 204)

@app.route('/deleteprocess', methods=['GET','POST'])
def deleteprocess():
    process_id = request.form.get('process_id')
    delprocess = PCDB_PRODUCT_PROCESS.query.filter(PCDB_PRODUCT_PROCESS.id==process_id).first()
    biib_code = delprocess.biib_code
    mfg_mode = delprocess.mfg_mode
    process_identifier = delprocess.process_identifier
    process_description = delprocess.process_description
    process_status = delprocess.process_status
    db.session.delete(delprocess)
    db.session.commit()
    row_id = process_id
    date_time = datetime.datetime.utcnow()
    table_name = "PCDB_PRODUCT_PROCESS"
    change_type = "DELETE"
    updated_value = ""
    column_name = "biib_code"
    original_value = biib_code
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    column_name = "mfg_mode"
    original_value = mfg_mode
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    column_name = "process_identifier"
    original_value = process_identifier
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    column_name = "process_description"
    original_value = process_description
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    column_name = "process_status"
    original_value = process_status
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    db.session.commit()
    return ('', 204)

@app.route('/getmeasuretable', methods=['GET','POST'])
def getmeasuretable():
    operation = request.form.get("operation_type")
    operation_id = operation.split('|')[0]
    sample_id = request.form.get("sample_id")
    pro_id = request.form.get('pro_id')
    process_id = pro_id.split(')')[0]
    results = PCDB_PARAMETERS_V.query.with_entities(PCDB_PARAMETERS_V.measure_id, PCDB_PARAMETERS_V.measure_name, PCDB_PARAMETERS_V.units_of_measure, PCDB_PARAMETERS_V.mapping_replicates, PCDB_PARAMETERS_V.id).filter(PCDB_PARAMETERS_V.process_id==process_id, PCDB_PARAMETERS_V.operation_id==operation_id, PCDB_PARAMETERS_V.sample_id==sample_id).all()
    measures = []
    for result in results:
        measure_id = result[0]
        measure_name = result[1]
        units_of_measure = result[2]
        mapping_replicates = result[3]
        parameter_id = result[4]
        #This is needed to jsonify data - give each data point a title so page can interpret data   
        measure = {
        "measure_id": measure_id,        
        "measure_name": measure_name, 
        "units_of_measure": units_of_measure,
        "mapping_replicates": mapping_replicates,
        "parameter_id": parameter_id,
            }
        measures.append(measure)
    return jsonify({'measures': measures})

@app.route('/updatemapreplicates', methods=['GET','POST'])
def updatemapreplicates():
    parameter_id = request.form.get('parameter_id')
    mapping_replicates = request.form.get('mapping_replicates')
    column_name = "MAPPING_REPLICATES"
    parameter = PCDB_PARAMETERS.query.filter(PCDB_PARAMETERS.id==parameter_id).first()
    oldmapreps = parameter.mapping_replicates
    if not oldmapreps:
        oldmapreps = ""
    parameter.mapping_replicates = mapping_replicates
    updated_value = mapping_replicates
    original_value = oldmapreps 
    db.session.commit()
    row_id = parameter_id
    date_time = datetime.datetime.utcnow()
    change_type = "EDIT"
    table_name = "PCDB_PARAMETERS"
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    db.session.commit()
    return ('', 204)

@app.route('/deletemeasure', methods=['GET','POST'])
def deletemeasure():
    parameter_id = request.form.get("parameter_id")
    delmeasure = PCDB_PARAMETERS.query.filter(PCDB_PARAMETERS.id==parameter_id).first()
    operation_id = delmeasure.operation_id
    sample_id = delmeasure.sample_id
    measure_id = delmeasure.measure_id
    sample_type = delmeasure.sample_type
    target = delmeasure.target
    lor = delmeasure.lor
    uor = delmeasure.uor
    lal = delmeasure.lal
    ual = delmeasure.ual
    lcl = delmeasure.lcl
    ucl = delmeasure.ucl
    lsl = delmeasure.lsl
    usl = delmeasure.usl
    classification = delmeasure.classification
    mapping_replicates = delmeasure.mapping_replicates
    db.session.delete(delmeasure)
    db.session.commit()
    row_id = parameter_id
    date_time = datetime.datetime.utcnow()
    table_name = "PCDB_PARAMETERS"
    change_type = "DELETE"
    updated_value = ""
    column_name = "operation_id"
    original_value = operation_id
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    column_name = "sample_id"
    original_value = sample_id
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    column_name = "measure_id"
    original_value = measure_id
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    column_name = "sample_type"
    original_value = sample_type
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    column_name = "target"
    original_value = target
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    column_name = "lor"
    original_value = lor
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    column_name = "uor"
    original_value = uor
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    column_name = "lal"
    original_value = lal
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    column_name = "ual"
    original_value = ual
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    column_name = "lcl"
    original_value = lcl
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    column_name = "ucl"
    original_value = ucl
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    column_name = "lsl"
    original_value = lsl
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    column_name = "usl"
    original_value = usl
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    column_name = "classification"
    original_value = classification
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    column_name = "mapping_replicates"
    original_value = mapping_replicates
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    db.session.commit()
    return ('', 204)

@app.route('/getoperation', methods=['GET','POST'])
def getoperation():
    #Get the biib_code from the form
    pro_id = request.form.get('pro_id')
    biib_code = request.form.get('biib_code')
    mfg_mode = pro_id.split('-')[0].split(")",1)[-1]
    process_identifier = pro_id.split('-',1)[-1]
    process_id = pro_id.split(')')[0]
    status_check = PCDB_PRODUCT_PROCESS.query.filter(PCDB_PRODUCT_PROCESS.id==process_id).first()
    process_status = status_check.process_status
    currentprocess = "(" + biib_code + ") " + mfg_mode + "-" + process_identifier + " (" + process_status + ")"
    #Need a blank list to add results into  
    op_choices = []
    results = PCDB_OPERATION.query.with_entities(PCDB_OPERATION.id, PCDB_OPERATION.operation_order, PCDB_OPERATION.operation_type, PCDB_OPERATION.operation_full_name).filter(PCDB_OPERATION.process_id==process_id).order_by(PCDB_OPERATION.operation_order.asc()).all()
   #Results are stored as a list of tuples - need to go through list and then unpack each tuple to get other data
    for result in results:
        operation_id = result[0]
        operation_order = result[1]
        operation_type = result[2]
        operation_full_name = result[3]
        #This is needed to jsonify data - give each data point a title so page can interpret data   
        opers = {
                "operation_id": operation_id,
                "operation_order": operation_order,
                "operation_type": operation_type,
                "operation_full_name": operation_full_name
                }
        op_choices.append(opers)
    return jsonify({'op_choices': op_choices, 'currentprocess': currentprocess})

@app.route('/addproduct',methods=['GET','POST'])
def addproduct():
    biib_code = str(request.form.get('biib_code_new'))
    partner = str(request.form.get('partner'))
    modality = str(request.form.get('modality'))
    output = biib_code + " " + partner + " " + modality
    if biib_code and partner and modality:
        exproducts = []
        results = list(PCDB_PRODUCT.query.with_entities(PCDB_PRODUCT.biib_code).all())
        for result in results:
            prod = result[0]
            exproducts.append(prod)
        if biib_code in exproducts:
            return jsonify({'output':'Product: ' + output + ' Already Exists'})
        else:    
            dbproduct = PCDB_PRODUCT(biib_code,partner,modality)
            db.session.add(dbproduct)
            db.session.commit()
            new_biib_choices = []
            results = PCDB_PRODUCT.query.with_entities(PCDB_PRODUCT.biib_code).all()
            for result in results:
                biib_code = result[0]
                product = {
                        "biib_code": biib_code
                        }
                new_biib_choices.append(product)
            date_time = datetime.datetime.utcnow()
            table_name = "PCDB_PRODUCT"
            change_type = "ADD"
            row_id = biib_code
            original_value = ""            
            column_name = "biib_code"
            updated_value = biib_code
            dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
            db.session.add(dbaudit)
            column_name = "partner"
            updated_value = partner
            dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
            db.session.add(dbaudit)
            column_name = "modality"
            updated_value = modality
            dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
            db.session.add(dbaudit)
            db.session.commit()
            return jsonify({'output':'Product: ' + output + ' has been added', 'new_biib_choices': new_biib_choices})
    else:
        return jsonify({'output' : 'Error Missing data!'})

@app.route('/addalias',methods=['GET','POST'])
def addalias():
    biib_code = str(request.form.get('biib_code_alias'))
    alias = str(request.form.get('alias'))
    alias_type = str(request.form.get('alias_type'))
    output = biib_code + " " + alias + " " + alias_type
    alias_check = PCDB_PRODUCT_ALIAS.query.filter(PCDB_PRODUCT_ALIAS.biib_code==biib_code, PCDB_PRODUCT_ALIAS.alias==alias).first()
    if alias_check is None:
        dbalias = PCDB_PRODUCT_ALIAS(biib_code,alias,alias_type)
        db.session.add(dbalias)
        newalias = PCDB_PRODUCT_ALIAS.query.filter(PCDB_PRODUCT_ALIAS.biib_code==biib_code, PCDB_PRODUCT_ALIAS.alias==alias, PCDB_PRODUCT_ALIAS.alias_type==alias_type).first()
        row_id = newalias.id
        date_time = datetime.datetime.utcnow()
        table_name = "PCDB_PRODUCT_ALIAS"
        change_type = "ADD"
        original_value = ""            
        column_name = "biib_code"
        updated_value = biib_code
        dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
        db.session.add(dbaudit)
        column_name = "alias"
        updated_value = alias
        dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
        db.session.add(dbaudit)
        column_name = "alias_type"
        updated_value = alias_type
        dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
        db.session.add(dbaudit)
        db.session.commit()
        return jsonify({'output':'Alias: ' + output + ' has been added'})
    else:    
        return jsonify({'output':'Alias: ' + output + ' Already Exists'})   
    
@app.route('/addprocess',methods=['GET','POST'])
def addprocess(): 
    biib_code = request.form.get('biib_code')
    mfg_mode = str(request.form.get('mfg_mode'))
    process_identifier = request.form.get('process_identifier')
    process_description = request.form.get('process_description')
    process_status = request.form.get('process_status')
    if process_description is None:
        process_description = " "
    output = biib_code + " " + mfg_mode + " " + process_identifier + " " + process_description + " " + process_status
    process_check = PCDB_PRODUCT_PROCESS.query.filter(PCDB_PRODUCT_PROCESS.biib_code==biib_code, PCDB_PRODUCT_PROCESS.mfg_mode==mfg_mode, PCDB_PRODUCT_PROCESS.process_identifier==process_identifier, PCDB_PRODUCT_PROCESS.process_status==process_status).first()
    if process_check is None:
        dbprocess = PCDB_PRODUCT_PROCESS(biib_code,mfg_mode,process_identifier,process_description,process_status)
        db.session.add(dbprocess)
        db.session.commit()
        newprocess = PCDB_PRODUCT_PROCESS.query.order_by(PCDB_PRODUCT_PROCESS.id.desc()).first() 
        row_id = newprocess.id
        date_time = datetime.datetime.utcnow()
        table_name = "PCDB_PROCESS"
        change_type = "ADD"
        original_value = ""            
        column_name = "biib_code"
        updated_value = biib_code
        dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
        db.session.add(dbaudit)
        column_name = "mfg_mode"
        updated_value = mfg_mode
        dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
        db.session.add(dbaudit)
        column_name = "process_identifier"
        updated_value = process_identifier
        dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
        db.session.add(dbaudit)
        column_name = "process_description"
        updated_value = process_description
        dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
        db.session.add(dbaudit)
        column_name = "process_status"
        updated_value = process_status
        dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
        db.session.add(dbaudit)
        db.session.commit()
        #Need a blank list to add results into
        new_pro_choices = []
        results = PCDB_PRODUCT_PROCESS.query.with_entities(PCDB_PRODUCT_PROCESS.id).filter(PCDB_PRODUCT_PROCESS.biib_code==biib_code, PCDB_PRODUCT_PROCESS.mfg_mode==mfg_mode, PCDB_PRODUCT_PROCESS.process_identifier==process_identifier, PCDB_PRODUCT_PROCESS.process_status==process_status).all()
        for result in results:
            process_id = result[0]
            process = {
                "id": process_id,
                "biib_code": biib_code,
                "mfg_mode": mfg_mode,
                "process_identifier": process_identifier,
                "process_status": process_status}
            new_pro_choices.append(process)
        return jsonify({'output':'Process: ' + output + ' has been added', 'new_pro_choices': new_pro_choices}) 
    else:    
        return jsonify({'output':'Process: ' + output + ' Already Exists'})

@app.route('/getmeasures', methods=['GET','POST'])
def getmeasures():
    meas_choices = []
    measures = PCDB_MEASURE.query.with_entities(PCDB_MEASURE.id, PCDB_MEASURE.measure_name, PCDB_MEASURE.units_of_measure).order_by(PCDB_MEASURE.measure_name.asc()).all()
    for measure in measures:
        measure_id = measure[0]
        measure_name = measure[1]
        units_of_measure = measure[2]
        exmeas = {
                "measure_name": measure_name,
                "measure_id": measure_id,
                "units_of_measure": units_of_measure
                }
        meas_choices.append(exmeas)
    return jsonify({'meas_choices': meas_choices})

@app.route('/getsample', methods=['GET','POST'])
def getsample():
    #Get the biib_code from the form
    operation = request.form.get('operation_type')
    operation_type = operation.split('|',1)[1]
    samp_choices = []
    results = PCDB_SAMPLE.query.with_entities(PCDB_SAMPLE.id, PCDB_SAMPLE.sample_order, PCDB_SAMPLE.sample_name).filter(PCDB_SAMPLE.operation_type==operation_type).order_by(PCDB_SAMPLE.sample_order.asc()).all()
   #Results are stored as a list of tuples - need to go through list and then unpack each tuple to get other data
    for result in results:
        sample_id = result[0]
        sample_order = result[1]
        sample_name = result[2]
        #This is needed to jsonify data - give each data point a title so page can interpret data   
        samps = {
                "sample_id": sample_id,
                "sample_order": sample_order,
                "sample_name": sample_name
                }
        samp_choices.append(samps)
    return jsonify({'samp_choices': samp_choices})

@app.route('/addnewmeasure',methods=['GET','POST'])
def addnewmeasure(): 
    measure_name = request.form.get('measure_name')
    units_of_measure = request.form.get('units_of_measure')
    data_type = request.form.get('data_type')
    if not measure_name:
        return jsonify({'measalert': "Can't be blank"})
    else:
        meas_check = PCDB_MEASURE.query.filter(PCDB_MEASURE.measure_name==measure_name, PCDB_MEASURE.units_of_measure==units_of_measure, PCDB_MEASURE.data_type==data_type).first()
        if meas_check is None:
            sample = PCDB_MEASURE(measure_name, units_of_measure, data_type)
            db.session.add(sample)
            db.session.commit()
            newmeasure = PCDB_MEASURE.query.filter(PCDB_MEASURE.measure_name==measure_name, PCDB_MEASURE.units_of_measure==units_of_measure, PCDB_MEASURE.data_type==data_type).first()
            row_id = newmeasure.id
            date_time = datetime.datetime.utcnow()
            table_name = "PCDB_MEASURE"
            change_type = "ADD"
            original_value = ""            
            column_name = "measure_name"
            updated_value = measure_name
            dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
            db.session.add(dbaudit)
            column_name = "units_of_measure"
            updated_value = units_of_measure
            dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
            db.session.add(dbaudit)
            column_name = "data_type"
            updated_value = data_type
            dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
            db.session.add(dbaudit)
            db.session.commit()
            newmeas_choices = []
            results = PCDB_MEASURE.query.with_entities(PCDB_MEASURE.id).filter(PCDB_MEASURE.measure_name==measure_name, PCDB_MEASURE.units_of_measure==units_of_measure, PCDB_MEASURE.data_type==data_type).all()
            #Results are stored as a list of tuples - need to go through list and then unpack each tuple to get other data
            for result in results:
                measure_id = result[0]
                #This is needed to jsonify data - give each data point a title so page can interpret data   
                meas = {
                        "measure_id": measure_id,
                        "measure_name": measure_name,
                        "units_of_measure": units_of_measure
                        }
                newmeas_choices.append(meas)
                measalert = "Measure: " + measure_name + " has been added"
                return jsonify({'measalert': measalert, 'newmeas_choices': newmeas_choices}) 
        else:
            return jsonify({'measalert': "Measure already exists"})

@app.route('/addmeasure',methods=['GET','POST'])
def addmeasure():
    operation= request.form.get("operation_type")
    operation_id = operation.split('|')[0]
    sample_id = request.form.get("sample_id")
    measures = request.form.getlist("measure_id[]")
    for measure in measures:
        measure_id = measure
        newmeasure = PCDB_PARAMETERS(operation_id,sample_id,measure_id)
        db.session.add(newmeasure)
        db.session.commit()
        newparam = PCDB_PARAMETERS.query.filter(PCDB_PARAMETERS.operation_id==operation_id, PCDB_PARAMETERS.sample_id==sample_id, PCDB_PARAMETERS.measure_id==measure_id).first()
        row_id = newparam.id
        date_time = datetime.datetime.utcnow()
        table_name = "PCDB_PARAMETERS"
        change_type = "ADD"
        original_value = ""            
        column_name = "operation_id"
        updated_value = operation_id
        dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
        db.session.add(dbaudit)
        column_name = "sample_id"
        updated_value = sample_id
        dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
        db.session.add(dbaudit)
        column_name = "measure_id"
        updated_value = measure_id
        dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
        db.session.add(dbaudit)
        db.session.commit()
    return jsonify({'output':'Measure has been added'})

@app.route('/addoperation',methods=['GET','POST'])
def addoperation(): 
    pro_id = request.form.get("pro_id")
    mfg_mode = pro_id.split('-')[0].split(")",1)[-1]
    process_id = pro_id.split(')')[0]
    biib_code = request.form.get("biib_code")
    operation_order = request.form.get("operation_order")
    operation_name = request.form.get("operation_name")
    operation_type = request.form.get("operation_type")
    chrom_type = request.form.get("chrom_type")
    adjustment_type = request.form.get("adjustment_type")
    adjustment_alias = request.form.get("adjustment_alias")
    cycle_pool = request.form.get("cycle_pool")
    bpr_order = request.form.get("bpr_order")
    if "NULL" in chrom_type:
        operation_full_name = operation_name
    else:
        operation_full_name = str(operation_name + "|" + cycle_pool + "|" + adjustment_type +"|" + adjustment_alias)
    oper_check = PCDB_OPERATION.query.filter(PCDB_OPERATION.biib_code==biib_code, PCDB_OPERATION.mfg_mode==mfg_mode, PCDB_OPERATION.operation_name==operation_name, PCDB_OPERATION.operation_type==operation_type).first()
    if oper_check is None:
        dbprocess = PCDB_OPERATION(process_id,biib_code,mfg_mode,operation_order,operation_name,operation_type,chrom_type,adjustment_type,adjustment_alias,cycle_pool,bpr_order,operation_full_name)
        db.session.add(dbprocess)
        db.session.commit()
        newoperation = PCDB_OPERATION.query.filter(PCDB_OPERATION.biib_code==biib_code, PCDB_OPERATION.mfg_mode==mfg_mode, PCDB_OPERATION.operation_name==operation_name, PCDB_OPERATION.operation_type==operation_type).first()
        row_id = newoperation.id
        date_time = datetime.datetime.utcnow()
        table_name = "PCDB_OPERATION"
        change_type = "ADD"
        original_value = ""            
        column_name = "biib_code"
        updated_value = biib_code
        dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
        db.session.add(dbaudit)
        column_name = "mfg_mode"
        updated_value = mfg_mode
        dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
        db.session.add(dbaudit)
        column_name = "operation_name"
        updated_value = operation_name
        dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
        db.session.add(dbaudit)
        column_name = "operation_type"
        updated_value = operation_type
        dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
        db.session.add(dbaudit)
        column_name = "chrom_type"
        updated_value = chrom_type
        dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
        db.session.add(dbaudit)
        column_name = "adjustment_type"
        updated_value = adjustment_type
        dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
        db.session.add(dbaudit)
        column_name = "adjustment_alias"
        updated_value = adjustment_alias
        dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
        db.session.add(dbaudit)
        column_name = "cycle_pool"
        updated_value = cycle_pool
        dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
        db.session.add(dbaudit)
        column_name = "bpr_order"
        updated_value = bpr_order
        dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
        db.session.add(dbaudit)
        db.session.commit()
        newop_choices = []
        results = PCDB_OPERATION.query.with_entities(PCDB_OPERATION.id, PCDB_OPERATION.operation_order, PCDB_OPERATION.operation_type, PCDB_OPERATION.operation_full_name).filter(PCDB_OPERATION.process_id==process_id, PCDB_OPERATION.operation_full_name==operation_full_name).all()
        #Results are stored as a list of tuples - need to go through list and then unpack each tuple to get other data
        for result in results:
            operation_id = result[0]
            operation_order = result[1]
            operation_type = result[2]
            operation_full_name = result[3]
                #This is needed to jsonify data - give each data point a title so page can interpret data   
            opers = {
                "operation_id": operation_id,
                "operation_order": operation_order,
                "operation_type": operation_type,
                "operation_full_name": operation_full_name
                    }
            newop_choices.append(opers)
            opalert = "Operation: " + operation_full_name + " has been added"
            return jsonify({'opalert': opalert, 'newop_choices': newop_choices})
    else:
        return jsonify({'opalert': "Operation already exists"})

@app.route('/addsample',methods=['GET','POST'])
def addsample(): 
    operation = request.form.get('operation_type')
    operation_type = operation.split('|',1)[1]
    sample_order = request.form.get('sample_order')
    sample_name = request.form.get('sample_name')
    samp_check = PCDB_SAMPLE.query.filter(PCDB_SAMPLE.operation_type==operation_type, PCDB_SAMPLE.sample_order==sample_order,PCDB_SAMPLE.sample_name==sample_name).first()
    if samp_check is None: 
        sample = PCDB_SAMPLE(operation_type, sample_order, sample_name)
        db.session.add(sample)
        db.session.commit()
        newsample = PCDB_SAMPLE.query.filter(PCDB_SAMPLE.operation_type==operation_type, PCDB_SAMPLE.sample_order==sample_order,PCDB_SAMPLE.sample_name==sample_name).first() 
        row_id = newsample.id
        date_time = datetime.datetime.utcnow()
        table_name = "PCDB_SAMPLE"
        change_type = "ADD"
        original_value = ""            
        column_name = "operation_type"
        updated_value = operation_type
        dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
        db.session.add(dbaudit)
        column_name = "sample_order"
        updated_value = sample_order
        dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
        db.session.add(dbaudit)
        column_name = "sample_name"
        updated_value = sample_name
        dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
        db.session.add(dbaudit)
        db.session.commit()
        newsamp_choices = []
        results = PCDB_SAMPLE.query.with_entities(PCDB_SAMPLE.id).filter(PCDB_SAMPLE.operation_type==operation_type, PCDB_SAMPLE.sample_order==sample_order, PCDB_SAMPLE.sample_name==sample_name).all()
        #Results are stored as a list of tuples - need to go through list and then unpack each tuple to get other data
        for result in results:
            sample_id = result[0]
            #This is needed to jsonify data - give each data point a title so page can interpret data   
            samps = {
                    "sample_id": sample_id,
                    "sample_order": sample_order,
                    "sample_name": sample_name
                    }
            newsamp_choices.append(samps)
        sampalert = "Sample: " + sample_name + " has been added"
        return jsonify({'sampalert': sampalert, 'newsamp_choices': newsamp_choices})
    else:
        return jsonify({'sampalert': "Sample already exists"})

@app.route('/getparam', methods=['GET','POST'])
def getparam():
    pro_id = request.form.get('pro_id')
    process_id = pro_id.split(')')[0]
    parameters = []
    results = PCDB_PARAMETERS_V.query.with_entities(PCDB_PARAMETERS_V.id, PCDB_PARAMETERS_V.operation_name, PCDB_PARAMETERS_V.operation_type, PCDB_PARAMETERS_V.sample_name, PCDB_PARAMETERS_V.measure_name, PCDB_PARAMETERS_V.units_of_measure, PCDB_PARAMETERS_V.sample_type, PCDB_PARAMETERS_V.target, PCDB_PARAMETERS_V.lor, PCDB_PARAMETERS_V.uor, PCDB_PARAMETERS_V.lal, PCDB_PARAMETERS_V.ual, PCDB_PARAMETERS_V.lsl, PCDB_PARAMETERS_V.usl, PCDB_PARAMETERS_V.lcl, PCDB_PARAMETERS_V.ucl, PCDB_PARAMETERS_V.classification, PCDB_PARAMETERS_V.operation_order, PCDB_PARAMETERS_V.sample_order, PCDB_PARAMETERS_V.parameter_name, PCDB_PARAMETERS_V.decimal_display).filter(PCDB_PARAMETERS_V.process_id==process_id).order_by(PCDB_PARAMETERS_V.operation_order.asc(), PCDB_PARAMETERS_V.sample_order.asc(), PCDB_PARAMETERS_V.measure_name.asc()).all()
    for result in results:
        id = result[0]
        operation_name = result[1]
        operation_type = result[2]
        sample_name = result[3]
        measure_name = result[4]
        units_of_measure = result[5]
        sample_type = result[6]
        target = result[7]
        lor = result[8]
        uor = result[9]
        lal = result[10]
        ual = result[11]
        lsl = result[12]
        usl = result[13]
        lcl = result[14]
        ucl = result[15]
        classification = result[16]
        parameter_name = result[19]
        decimal_display = result[20]
        params = {
                "id": id, 
                "operation_name": operation_name, 
                "operation_type": operation_type, 
                "sample_name": sample_name, 
                "measure_name": measure_name, 
                "units_of_measure": units_of_measure, 
                "sample_type": sample_type, 
                "target": target, 
                "lor": lor, 
                "uor": uor, 
                "lal": lal, 
                "ual": ual, 
                "lsl": lsl, 
                "usl": usl, 
                "lcl": lcl, 
                "ucl": ucl, 
                "classification": classification,
                "parameter_name": parameter_name,
                "decimal_display": decimal_display
                }
        parameters.append(params)
    return jsonify({'parameters':parameters})
    
@app.route('/updateparam', methods=['GET','POST'])
def updateparam():
    param_id = request.form.get('param_id')
    parameter_name = request.form.get('parameter_name')
    decimal_display = request.form.get('decimal_display')
    target = request.form.get('target')
    lor = request.form.get('lor')
    uor = request.form.get('uor')
    lal = request.form.get('lal')
    ual = request.form.get('ual')
    lsl = request.form.get('lsl')
    usl = request.form.get('usl')
    lcl = request.form.get('lcl')
    ucl = request.form.get('ucl')
    classification = request.form.get('classification')
    column_name = request.form.get('column_name')
    parameter = PCDB_PARAMETERS.query.filter(PCDB_PARAMETERS.id==param_id).first()
    try:
        oldtarget = parameter.target
    except:
        oldtarget = ""
    try:
        oldlor = parameter.lor
    except:
        oldlor = ""
    try:
        olduor = parameter.uor
    except:
        olduor = ""
    try:
        oldlal = parameter.lal
    except:
        oldlal = ""
    try:
        oldual = parameter.ual
    except:
        oldual = ""
    try:
        oldlcl = parameter.lcl
    except: 
        oldlcl = ""
    try:
        olducl = parameter.ucl
    except:
        olducl = ""
    try:
        oldlsl = parameter.lsl
    except:
        oldlsl = ""
    try:
        oldusl = parameter.usl
    except:
        oldusl = ""
    try:
        oldclass = parameter.classification    
    except:
        oldclass = ""
    try:
        oldparametername = parameter.parameter_name
    except:
        oldparametername = ""
    try:
        olddecimaldisplay = parameter.decimal_display
    except:
        olddecimaldisplay = ""
    if column_name == "target":
        if not target:
            parameter.target = None
            updated_value = target
            original_value = oldtarget
        else:
            parameter.target = target
            updated_value = target
            original_value = oldtarget
    if column_name == "lor":
        if not lor:
            parameter.lor = None
            updated_value = lor
            original_value = oldlor
        else:
            parameter.lor = lor
            updated_value = lor
            original_value = oldlor
    if column_name == "uor":
        if not uor:
            parameter.uor = None
            updated_value = uor
            original_value = olduor
        else:
            parameter.uor = uor
            updated_value = uor
            original_value = olduor
    if column_name == "lal":
        if not lal:
            parameter.lal = None
            updated_value = lal
            original_value = oldlal
        else:
            parameter.lal = lal
            updated_value = lal
            original_value = oldlal
    if column_name == "ual":
        if not ual:
            parameter.ual = None
            updated_value = ual
            original_value = oldual
        else:
            parameter.ual = ual
            updated_value = ual
            original_value = oldual
    if column_name == "lsl":
        if not lsl:
            parameter.lsl = None
            updated_value = lal
            original_value = oldlsl
        else:
            parameter.lsl = lsl
            updated_value = lal
            original_value = oldlsl
    if column_name == "usl":
        if not usl:
            parameter.usl = None
            updated_value = usl
            original_value = oldusl  
        else:
            parameter.usl = usl
            updated_value = usl
            original_value = oldusl  
    if column_name == "lcl":
        if not lcl:
            parameter.lcl = None
            updated_value = lcl
            original_value = oldlcl
        else:
            parameter.lcl = lcl
            updated_value = lcl
            original_value = oldlcl
    if column_name == "ucl":
        if not ucl:
            parameter.ucl = None
            updated_value = ucl
            original_value = olducl
        else:
            parameter.ucl = ucl
            updated_value = ucl
            original_value = olducl
    if column_name == "classification":
        if not classification:
            parameter.classification = None
            updated_value = classification
            original_value = oldclass    
        else:
            parameter.classification = classification
            updated_value = classification
            original_value = oldclass
    if column_name == "parameter_name":
        if not parameter_name:
            parameter.parameter_name = None
            updated_value = parameter_name
            original_value = oldparametername
        else:
            parameter.parameter_name = parameter_name
            updated_value = parameter_name
            original_value = oldparametername
    if column_name == "decimal_display":
        if not decimal_display:
            parameter.decimal_display = None
            updated_value = decimal_display
            original_value = olddecimaldisplay
        else:
            parameter.decimal_display = decimal_display
            updated_value = decimal_display
            original_value = olddecimaldisplay
    db.session.commit()
    row_id = param_id
    date_time = datetime.datetime.utcnow()
    change_type = "EDIT"
    table_name = "PCDB_PARMETER"
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    db.session.commit()
    return ('', 204)

@app.route('/deleteparam', methods=['GET','POST'])
def deleteparam():
    param_id = request.form.get('param_id')
    parameter = PCDB_PARAMETERS.query.filter(PCDB_PARAMETERS.id==param_id).first()
    operation_id = parameter.operation_id
    sample_id = parameter.sample_id
    measure_id = parameter.measure_id
    sample_type = parameter.sample_type
    parameter_name = parameter.parameter_name
    decimal_display = parameter.decimal_display
    target = parameter.target
    lor = parameter.lor
    uor = parameter.uor
    lal = parameter.lal
    ual = parameter.ual
    lcl = parameter.lcl
    ucl = parameter.ucl
    lsl = parameter.lsl
    usl = parameter.usl
    classification = parameter.classification
    db.session.delete(parameter)
    db.session.commit()    
    row_id = param_id
    date_time = datetime.datetime.utcnow()
    table_name = "PCDB_PARAMETERS"
    change_type = "DELETE"
    updated_value = ""
    column_name = "operation_id"
    original_value = operation_id
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    column_name = "sample_id"
    original_value = sample_id
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    column_name = "measure_id"
    original_value = measure_id
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    column_name = "sample_type"
    original_value = sample_type
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    column_name = "parameter_name"
    original_value = parameter_name
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    column_name = "decimal_display"
    original_value = decimal_display
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    column_name = "target"
    original_value = target
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    column_name = "lor"
    original_value = lor
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    column_name = "uor"
    original_value = uor
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    column_name = "lal"
    original_value = lal
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    column_name = "ual"
    original_value = ual
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    column_name = "lcl"
    original_value = lcl
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    column_name = "ucl"
    original_value = ucl
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    column_name = "lsl"
    original_value = lsl
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    column_name = "usl"
    original_value = usl
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    column_name = "classification"
    original_value = classification
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    db.session.commit()
    return ('', 204)   

@app.route('/getprimr', methods=['GET','POST'])
def getprimr():
    #ITS ADDING IT BEFORE ITS FINISHED THE LOOP SO IT ADDS ITS THEN FINDS THE NEXT PARAMETR AND ADDS IT AGAIN
    pro_id = request.form.get('pro_id')
    process_id = pro_id
    primrs = []
    map_reps = []
    mapresults = PCDB_PRIMR_V.query.with_entities(PCDB_PRIMR_V.parameter_id).all()
    for mapresult in mapresults:
        map_reps.append(mapresult[0])
    results = PCDB_PRIMR_V.query.with_entities(PCDB_PRIMR_V.parameter_id, PCDB_PRIMR_V.operation_name, PCDB_PRIMR_V.operation_type, PCDB_PRIMR_V.sample_name, PCDB_PRIMR_V.measure_name, PCDB_PRIMR_V.units_of_measure, PCDB_PRIMR_V.parameter_iteration, PCDB_PRIMR_V.primr_book_template_name, PCDB_PRIMR_V.primr_page_name, PCDB_PRIMR_V.primr_field_name, PCDB_PRIMR_V.primr_row_number, PCDB_PRIMR_V.mapping_id, PCDB_PRIMR_V.adjustment_type, PCDB_PRIMR_V.mapping_replicates).filter(PCDB_PRIMR_V.process_id==process_id).all()    
    for result in results:
        parameter_id = result[0]
        operation_name = result[1]
        operation_type = result[2]
        sample_name = result[3]
        measure_name = result[4]
        units_of_measure = result[5]
        parameter_iteration = result[6]
        primr_book_template_name = result[7]
        primr_page_name = result[8]
        primr_field_name = result[9]
        primr_row_number = result[10]
        mapping_id = result[11]
        adjustment_type = result[12]
        str_mapping_replicates = result[13]
        params = {
                "parameter_id": parameter_id, 
                "mapping_id": mapping_id,
                "operation_name": operation_name, 
                "operation_type": operation_type, 
                "adjustment_type": adjustment_type,
                "sample_name": sample_name, 
                "measure_name": measure_name, 
                "units_of_measure": units_of_measure, 
                "parameter_iteration": parameter_iteration,
                "primr_book_template_name": primr_book_template_name,
                "primr_page_name": primr_page_name,
                "primr_field_name": primr_field_name,
                "primr_row_number": primr_row_number, 
                "str_mapping_replicates": str_mapping_replicates
                }
        primrs.append(params)
        if not str_mapping_replicates:
            pass
        else:
            num_replicates = map_reps.count(parameter_id) - 1
            num_to_add = int(str_mapping_replicates) - num_replicates
            for x in range(0,num_to_add):
                params = {
                            "parameter_id": parameter_id, 
                            "mapping_id": None,
                            "operation_name": operation_name, 
                            "operation_type": operation_type,
                            "adjustment_type": adjustment_type,
                            "sample_name": sample_name, 
                            "measure_name": measure_name, 
                            "units_of_measure": units_of_measure, 
                            "parameter_iteration": None,
                            "primr_book_template_name": primr_book_template_name,
                            "primr_page_name": primr_page_name,
                            "primr_field_name": primr_field_name,
                            "primr_row_number": primr_row_number, 
                            "str_mapping_replicates": str_mapping_replicates
                            }
                map_reps.append(parameter_id)
                primrs.append(params)
    return jsonify({'primrs':primrs})
    
@app.route('/updateprimr', methods=['GET','POST'])
def updateprimr():
    parameter_iteration = request.form.get('parameter_iteration')
    parameter_id = request.form.get('parameter_id')
    mapping_id = request.form.get('mapping_id')
    primr_book_template_name = request.form.get('primr_book_template_name')
    primr_page_name = request.form.get('primr_page_name')
    primr_field_name = request.form.get('primr_field_name')
    primr_row_number = request.form.get('primr_row_number')
    column_name = request.form.get('column_name')
    if not mapping_id:   
        primr = PCDB_PRIMR(parameter_id,parameter_iteration,primr_book_template_name,primr_page_name,primr_field_name,primr_row_number)
        db.session.add(primr)
        db.session.commit()
        newprimr = PCDB_PRIMR.query.filter(PCDB_PRIMR.parameter_id==parameter_id).order_by(PCDB_PRIMR.id.desc()).first() 
        mapping_id = newprimr.id
        column_name = "parameter_id"
        updated_value = parameter_id
        original_value = ""
        row_id = mapping_id
        date_time = datetime.datetime.utcnow()
        change_type = "ADD"
        table_name = "PCDB_PRIMR"
        dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
        db.session.add(dbaudit)
        db.session.commit()
        parameter_iteration = request.form.get('parameter_iteration')
        primr = PCDB_PRIMR.query.filter(PCDB_PRIMR.id==mapping_id).first()
        oldparameter_iteration = primr.parameter_iteration
        oldprimr_book_template_name = primr.primr_book_template_name
        oldprimr_page_name = primr.primr_page_name
        oldprimr_field_name = primr.primr_field_name
        oldprimr_row_number = primr.primr_row_number
        if column_name == "parameter_iteration":
            primr.parameter_iteration = parameter_iteration
            updated_value = parameter_iteration
            original_value = oldparameter_iteration
        if column_name == "primr_book_template_name":
            primr.primr_book_template_name = primr_book_template_name
            updated_value = primr_book_template_name
            original_value = oldprimr_book_template_name
        if column_name == "primr_page_name":
            primr.primr_page_name = primr_page_name
            updated_value = primr_page_name
            original_value = oldprimr_page_name
        if column_name == "primr_field_name":
            primr.primr_field_name = primr_field_name
            updated_value = primr_field_name
            original_value = oldprimr_field_name
        if column_name == "primr_row_number":
            primr.primr_row_number = primr_row_number
            updated_value = primr_row_number
            original_value = oldprimr_row_number       
        db.session.commit()
        date_time = datetime.datetime.utcnow()
        change_type = "EDIT"
        table_name = "PCDB_PRIMR"
        dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
        db.session.add(dbaudit)
        db.session.commit()
        return ('', 204)   
    else:
        parameter_iteration = request.form.get('parameter_iteration')
        primr_book_template_name = request.form.get('primr_book_template_name')
        primr_page_name = request.form.get('primr_page_name')
        primr_field_name = request.form.get('primr_field_name')
        primr_row_number = request.form.get('primr_row_number')
        column_name = request.form.get('column_name')
        primr = PCDB_PRIMR.query.filter(PCDB_PRIMR.id==mapping_id).first()
        oldparameter_iteration = primr.parameter_iteration
        oldprimr_book_template_name = primr.primr_book_template_name
        oldprimr_page_name = primr.primr_page_name
        oldprimr_field_name = primr.primr_field_name
        oldprimr_row_number = primr.primr_row_number
        if column_name == "parameter_iteration":
            primr.parameter_iteration = parameter_iteration
            updated_value = parameter_iteration
            original_value = oldparameter_iteration
        if column_name == "primr_book_template_name":
            primr.primr_book_template_name = primr_book_template_name
            updated_value = primr_book_template_name
            original_value = oldprimr_book_template_name
        if column_name == "primr_page_name":
            primr.primr_page_name = primr_page_name
            updated_value = primr_page_name
            original_value = oldprimr_page_name
        if column_name == "primr_field_name":
            primr.primr_field_name = primr_field_name
            updated_value = primr_field_name
            original_value = oldprimr_field_name
        if column_name == "primr_row_number":
            primr.primr_row_number = primr_row_number
            updated_value = primr_row_number
            original_value = oldprimr_row_number       
        db.session.commit()
        row_id = mapping_id
        date_time = datetime.datetime.utcnow()
        change_type = "EDIT"
        table_name = "PCDB_PRIMR"
        dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
        db.session.add(dbaudit)
        db.session.commit()
        return ('', 204)   

@app.route('/deleteprimr', methods=['GET','POST'])
def deleteprimr():
    mapping_id = request.form.get('mapping_id')
    parameter_id = request.form.get('parameter_id')
    parameter = PCDB_PRIMR.query.filter(PCDB_PRIMR.id==mapping_id).first()
    parameter_iteration = parameter.parameter_iteration
    if not parameter_iteration:
        parameter_iteration = ""
    primr_book_template_name = parameter.primr_book_template_name
    primr_page_name = parameter.primr_page_name
    primr_field_name = parameter.primr_field_name
    primr_row_number = parameter.primr_row_number
    db.session.delete(parameter)
    db.session.commit()    
    row_id = mapping_id
    date_time = datetime.datetime.utcnow()
    table_name = "PCDB_PRIMR"
    change_type = "DELETE"
    updated_value = ""
    column_name = "parameter_id"
    original_value = parameter_id
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    column_name = "parameter_iteration"
    original_value = parameter_iteration
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    column_name = "primr_book_template_name"
    original_value = primr_book_template_name
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    column_name = "primr_page_name"
    original_value = primr_page_name
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    column_name = "primr_field_name"
    original_value = primr_field_name
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    column_name = "primr_row_number"
    original_value = primr_row_number
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    db.session.commit()
    return ('', 204)   


@app.route('/getdeltav', methods=['GET','POST'])
def getdeltav():
    pro_id = request.form.get('pro_id')
    process_id = pro_id
    deltavs = []
    map_reps = []
    mapresults = PCDB_DELTAV_V.query.with_entities(PCDB_DELTAV_V.parameter_id).all()
    for mapresult in mapresults:
        map_reps.append(mapresult[0])
    results = PCDB_DELTAV_V.query.with_entities(PCDB_DELTAV_V.parameter_id, PCDB_DELTAV_V.operation_name, PCDB_DELTAV_V.operation_type, PCDB_DELTAV_V.sample_name, PCDB_DELTAV_V.measure_name, PCDB_DELTAV_V.units_of_measure, PCDB_DELTAV_V.dv_unit, PCDB_DELTAV_V.dv_process_phase, PCDB_DELTAV_V.dv_category, PCDB_DELTAV_V.dv_cycle, PCDB_DELTAV_V.dv_xlt_process_name, PCDB_DELTAV_V.dv_unit_procedure, PCDB_DELTAV_V.dv_iteration, PCDB_DELTAV_V.dv_output_col, PCDB_DELTAV_V.mapping_id, PCDB_DELTAV_V.adjustment_type, PCDB_DELTAV_V.mapping_replicates).filter(PCDB_DELTAV_V.process_id==process_id).all()    
    for result in results:
        parameter_id = result[0]
        operation_name = result[1]
        operation_type = result[2]
        sample_name = result[3]
        measure_name = result[4]
        units_of_measure = result[5]
        dv_unit = result[6]
        dv_process_phase = result[7]
        dv_category = result[8]
        dv_cycle = result[9]
        dv_xlt_process_name = result[10]
        dv_unit_procedure = result[11]
        dv_iteration = result[12]
        dv_output_col = result[13]
        mapping_id = result[14]
        adjustment_type = result[15]
        str_mapping_replicates = result[16]
        params = {
                "mapping_id": mapping_id,
                "parameter_id": parameter_id, 
                "operation_name": operation_name, 
                "operation_type": operation_type, 
                "adjustment_type": adjustment_type,
                "sample_name": sample_name, 
                "measure_name": measure_name, 
                "units_of_measure": units_of_measure, 
                "dv_unit": dv_unit,
                "dv_process_phase": dv_process_phase,
                "dv_category": dv_category,
                "dv_cycle": dv_cycle,
                "dv_xlt_process_name": dv_xlt_process_name,
                "dv_unit_procedure": dv_unit_procedure,
                "dv_iteration": dv_iteration,
                "dv_output_col": dv_output_col,
                "str_mapping_replicates": str_mapping_replicates,
                    }
        deltavs.append(params)
        if not str_mapping_replicates:
            pass
        else:
            num_replicates = map_reps.count(parameter_id) - 1
            num_to_add = int(str_mapping_replicates) - num_replicates
            for x in range(0,num_to_add):
                params = {
                        "mapping_id": None,
                        "parameter_id": parameter_id, 
                        "operation_name": operation_name, 
                        "operation_type": operation_type, 
                        "adjustment_type": adjustment_type,
                        "sample_name": sample_name, 
                        "measure_name": measure_name, 
                        "units_of_measure": units_of_measure, 
                        "dv_unit": dv_unit,
                        "dv_process_phase": dv_process_phase,
                        "dv_category": dv_category,
                        "dv_cycle": dv_cycle,
                        "dv_xlt_process_name": dv_xlt_process_name,
                        "dv_unit_procedure": dv_unit_procedure,
                        "dv_iteration": dv_iteration,
                        "dv_output_col": dv_output_col,
                        "str_mapping_replicates": str_mapping_replicates,
                        }
                map_reps.append(parameter_id)
                deltavs.append(params)
    return jsonify({'deltavs':deltavs})
    
@app.route('/updatedeltav', methods=['GET','POST'])
def updatedeltav():
    parameter_id = request.form.get('parameter_id')
    mapping_id = request.form.get('mapping_id')
    dv_unit = request.form.get('dv_unit')
    dv_process_phase = request.form.get('dv_process_phase')
    dv_category = request.form.get('dv_category')
    dv_cycle = request.form.get('dv_cycle')
    dv_xlt_process_name = request.form.get('dv_xlt_process_name')
    dv_unit_procedure = request.form.get('dv_unit_procedure')
    dv_iteration = request.form.get('dv_iteration')
    dv_output_col = request.form.get('dv_output_col')
    column_name = request.form.get('column_name')
    if not mapping_id:   
        deltav = PCDB_DELTAV(parameter_id,dv_unit,dv_process_phase,dv_category,dv_cycle,dv_xlt_process_name,dv_unit_procedure,dv_iteration,dv_output_col)
        db.session.add(deltav)
        db.session.commit()
        newdeltav = PCDB_DELTAV.query.filter(PCDB_DELTAV.parameter_id==parameter_id).order_by(PCDB_DELTAV.id.desc()).first() 
        mapping_id = newdeltav.id
        column_name = "parameter_id"
        updated_value = parameter_id
        original_value = ""
        row_id = mapping_id
        date_time = datetime.datetime.utcnow()
        change_type = "ADD"
        table_name = "PCDB_DELTAV"
        dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
        db.session.add(dbaudit)
        db.session.commit()
        deltav = PCDB_DELTAV.query.filter(PCDB_DELTAV.id==mapping_id).first()
        olddv_unit = deltav.dv_unit
        olddv_process_phase = deltav.dv_process_phase
        olddv_category = deltav.dv_category
        olddv_cycle = deltav.dv_cycle
        olddv_xlt_process_name = deltav.dv_xlt_process_name
        olddv_unit_procedure = deltav.dv_unit_procedure
        olddv_iteration = deltav.dv_iteration
        olddv_output_col = deltav.dv_output_col
        if column_name == "dv_unit":
            deltav.dv_unit = dv_unit
            updated_value = dv_unit
            original_value = olddv_unit
        if column_name == "dv_process_phase":
            deltav.dv_process_phase = dv_process_phase
            updated_value = dv_process_phase
            original_value = olddv_process_phase
        if column_name == "dv_category":
            deltav.dv_category = dv_category
            updated_value = dv_category
            original_value = olddv_category
        if column_name == "dv_cycle":
            deltav.dv_cycle = dv_cycle
            updated_value = dv_cycle
            original_value = olddv_cycle
        if column_name == "dv_xlt_process_name":
            deltav.dv_xlt_process_name = dv_xlt_process_name
            updated_value = dv_xlt_process_name
            original_value = olddv_xlt_process_name
        if column_name == "dv_unit_procedure":
            deltav.dv_unit_procedure = dv_unit_procedure
            updated_value = dv_unit_procedure
            original_value = olddv_unit_procedure
        if column_name == "dv_iteration":
            deltav.dv_iteration = dv_iteration
            updated_value = dv_iteration
            original_value = olddv_iteration
        if column_name == "dv_output_col":
            deltav.dv_output_col = dv_output_col
            updated_value = dv_output_col
            original_value = olddv_output_col
        db.session.commit()
        date_time = datetime.datetime.utcnow()
        change_type = "EDIT"
        table_name = "PCDB_DELTAV"
        dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
        db.session.add(dbaudit)
        db.session.commit()
        return ('', 204)   
    else:
        deltav = PCDB_DELTAV.query.filter(PCDB_DELTAV.id==mapping_id).first()
        olddv_unit = deltav.dv_unit
        olddv_process_phase = deltav.dv_process_phase
        olddv_category = deltav.dv_category
        olddv_cycle = deltav.dv_cycle
        olddv_xlt_process_name = deltav.dv_xlt_process_name
        olddv_unit_procedure = deltav.dv_unit_procedure
        olddv_iteration = deltav.dv_iteration
        olddv_output_col = deltav.dv_output_col
        if column_name == "dv_unit":
            deltav.dv_unit = dv_unit
            updated_value = dv_unit
            original_value = olddv_unit
        if column_name == "dv_process_phase":
            deltav.dv_process_phase = dv_process_phase
            updated_value = dv_process_phase
            original_value = olddv_process_phase
        if column_name == "dv_category":
            deltav.dv_category = dv_category
            updated_value = dv_category
            original_value = olddv_category
        if column_name == "dv_cycle":
            deltav.dv_cycle = dv_cycle
            updated_value = dv_cycle
            original_value = olddv_cycle
        if column_name == "dv_xlt_process_name":
            deltav.dv_xlt_process_name = dv_xlt_process_name
            updated_value = dv_xlt_process_name
            original_value = olddv_xlt_process_name
        if column_name == "dv_unit_procedure":
            deltav.dv_unit_procedure = dv_unit_procedure
            updated_value = dv_unit_procedure
            original_value = olddv_unit_procedure
        if column_name == "dv_iteration":
            deltav.dv_iteration = dv_iteration
            updated_value = dv_iteration
            original_value = olddv_iteration
        if column_name == "dv_output_col":
            deltav.dv_output_col = dv_output_col
            updated_value = dv_output_col
            original_value = olddv_output_col
        db.session.commit()
        row_id = mapping_id
        date_time = datetime.datetime.utcnow()
        change_type = "EDIT"
        table_name = "PCDB_DELTAV"
        dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
        db.session.add(dbaudit)
        db.session.commit()
        return ('', 204)   
    
@app.route('/deletedeltav', methods=['GET','POST'])
def deletedeltav():
    mapping_id = request.form.get('mapping_id')
    parameter_id = request.form.get('parameter_id')
    parameter = PCDB_DELTAV.query.filter(PCDB_DELTAV.id==mapping_id).first()
    dv_unit = parameter.dv_unit
    dv_process_phase = parameter.dv_process_phase
    dv_category = parameter.dv_category
    dv_cycle = parameter.dv_cycle
    dv_xlt_process_name = parameter.dv_xlt_process_name
    dv_unit_procedure = parameter.dv_category
    dv_iteration = parameter.dv_cycle
    dv_output_col = parameter.dv_xlt_process_name    
    db.session.delete(parameter)
    db.session.commit()    
    row_id = mapping_id
    date_time = datetime.datetime.utcnow()
    table_name = "PCDB_DELTAV"
    change_type = "DELETE"
    updated_value = ""
    column_name = "dv_unit"
    original_value = dv_unit
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    column_name = "dv_process_phase"
    original_value = dv_process_phase
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    column_name = "dv_category"
    original_value = dv_category
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    column_name = "dv_cycle"
    original_value = dv_cycle
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    column_name = "dv_xlt_process_name"
    original_value = dv_xlt_process_name
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    column_name = "dv_unit_procedure"
    original_value = dv_unit_procedure
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    column_name = "dv_iteration"
    original_value = dv_iteration
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    column_name = "dv_output_col"
    original_value = dv_output_col
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    column_name = "parameter_id"
    original_value = parameter_id
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    db.session.commit()
    return ('', 204)   

@app.route('/geteditops', methods=['GET','POST'])
def geteditops():
    pro_id = request.form.get('pro_id') 
    process_id = pro_id.split(')')[0]
    editops = []
    results = PCDB_OPERATION.query.with_entities(PCDB_OPERATION.id, PCDB_OPERATION.operation_order, PCDB_OPERATION.operation_type, PCDB_OPERATION.operation_name, PCDB_OPERATION.chrom_type, PCDB_OPERATION.adjustment_type, PCDB_OPERATION.adjustment_alias, PCDB_OPERATION.cycle_pool, PCDB_OPERATION.bpr_order).filter(PCDB_OPERATION.process_id==process_id).order_by(PCDB_OPERATION.operation_order.asc()).all()
    for result in results:
        operation_id = result[0]
        operation_order = result[1]
        operation_type = result[2]
        operation_name = result[3]
        chrom_type = result[4]
        adjustment_type = result[5]
        adjustment_alias = result[6]
        cycle_pool = result[7]
        bpr_order = result[8]
        #This is needed to jsonify data - give each data point a title so page can interpret data   
        params = {
        "operation_id": operation_id,
        "operation_order": operation_order, 
        "operation_type": operation_type, 
        "operation_name": operation_name, 
        "chrom_type": chrom_type, 
        "adjustment_type": adjustment_type, 
        "adjustment_alias": adjustment_alias, 
        "cycle_pool": cycle_pool,
        "bpr_order": bpr_order
            }
        editops.append(params)
    return jsonify({'editops':editops})
    
@app.route('/updateeditops', methods=['GET','POST'])
def updateeditops():
    operation_id = request.form.get('operation_id')
    operation_order = request.form.get('operation_order')
    operation_type = request.form.get('operation_type')
    operation_name = request.form.get('operation_name')
    chrom_type = request.form.get('chrom_type')
    adjustment_type = request.form.get('adjustment_type')
    adjustment_alias = request.form.get('adjustment_alias')
    cycle_pool = request.form.get('cycle_pool')
    bpr_order = request.form.get('bpr_order')
    column_name = request.form.get('column_name')
    if "NULL" in chrom_type:
        operation_full_name = operation_name
    else:
        operation_full_name = str(operation_name + "|" + cycle_pool + "|" + adjustment_type +"|" + adjustment_alias)
    editops = PCDB_OPERATION.query.filter(PCDB_OPERATION.id==operation_id).first()
    oldoperation_order = editops.operation_order
    oldoperation_type = editops.operation_type
    oldoperation_name = editops.operation_name
    oldchrom_type = editops.chrom_type
    oldadjustment_type = editops.adjustment_type
    oldadjustment_alias = editops.adjustment_alias
    oldcycle_pool = editops.cycle_pool
    oldbpr_order = editops.bpr_order   
    if column_name == "operation_order":
        editops.operation_order = operation_order
        updated_value = operation_order
        original_value = oldoperation_order
    if column_name == "operation_type":
        editops.operation_type = operation_type
        updated_value = operation_type
        original_value = oldoperation_type
    if column_name == "operation_name":
        editops.operation_name = operation_name
        editops.operation_full_name = operation_full_name
        updated_value = operation_name
        original_value = oldoperation_name
    if column_name == "chrom_type":
        editops.chrom_type = chrom_type
        editops.operation_full_name = operation_full_name
        updated_value = chrom_type
        original_value = oldchrom_type
    if column_name == "adjustment_type":
        editops.adjustment_type = adjustment_type
        editops.operation_full_name = operation_full_name
        updated_value = adjustment_type
        original_value = oldadjustment_type
    if column_name == "adjustment_alias":
        editops.adjustment_alias = adjustment_alias
        editops.operation_full_name = operation_full_name
        updated_value = adjustment_alias
        original_value = oldadjustment_alias
    if column_name == "cycle_pool":
        editops.cycle_pool = cycle_pool
        editops.operation_full_name = operation_full_name
        updated_value = cycle_pool
        original_value = oldcycle_pool
    if column_name == "bpr_order":
        editops.bpr_order = bpr_order
        updated_value = bpr_order
        original_value = oldbpr_order
    db.session.commit()
    row_id = operation_id
    date_time = datetime.datetime.utcnow()
    change_type = "EDIT"
    table_name = "PCDB_OPERATION"
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    db.session.commit()
    return ('', 204)

@app.route('/deleteeditops', methods=['GET','POST'])
def deleteeditops():
    operation_id = request.form.get('operation_id')
    parameter = PCDB_OPERATION.query.filter(PCDB_OPERATION.id==operation_id).first()
    operation_order = parameter.operation_order
    operation_type = parameter.operation_type
    operation_name = parameter.operation_name
    chrom_type = parameter.chrom_type
    adjustment_type = parameter.adjustment_type
    adjustment_alias = parameter.adjustment_alias
    cycle_pool = parameter.cycle_pool
    bpr_order = parameter.bpr_order   
    db.session.delete(parameter)
    db.session.commit()
    row_id = operation_id
    date_time = datetime.datetime.utcnow()
    table_name = "PCDB_OPERATION"
    change_type = "DELETE"
    updated_value = ""
    column_name = "operation_order"
    original_value = operation_order
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    column_name = "operation_type"
    original_value = operation_type
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    column_name = "operation_name"
    original_value = operation_name
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    column_name = "chrom_type"
    original_value = chrom_type
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    column_name = "adjustment_type"
    original_value = adjustment_type
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    column_name = "adjustment_alias"
    original_value = adjustment_alias
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    column_name = "cycle_pool"
    original_value = cycle_pool
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    column_name = "bpr_order"
    original_value = bpr_order
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    db.session.commit()
    return ('', 204)  

@app.route('/geteditsamps', methods=['GET','POST'])
def geteditsamps():
    #Get the biib_code from the form
    operation = request.form.get('operation_type')
    operation_type = operation.split('|',1)[1]
    editsamps = []
    results = PCDB_SAMPLE.query.with_entities(PCDB_SAMPLE.id, PCDB_SAMPLE.operation_type, PCDB_SAMPLE.sample_order, PCDB_SAMPLE.sample_name).filter(PCDB_SAMPLE.operation_type==operation_type).order_by(PCDB_SAMPLE.sample_order.asc()).all()
    #Results are stored as a list of tuples - need to go through list and then unpack each tuple to get other data
    for result in results:
        sample_id = result[0]
        operation_type = result[1]
        sample_order = result[2]
        sample_name = result[3]
        #This is needed to jsonify data - give each data point a title so page can interpret data   
        samps = {
                "sample_id": sample_id,
                "operation_type": operation_type,
                "sample_order": sample_order,
                "sample_name": sample_name
                }
        editsamps.append(samps)
    return jsonify({'editsamps':editsamps})
    
@app.route('/updateeditsamps', methods=['GET','POST'])
def updateeditsamps():
    sample_id = request.form.get('sample_id')
    sample_order = request.form.get('sample_order')
    sample_name = request.form.get('sample_name')
    column_name = request.form.get('column_name')
    editsamps = PCDB_SAMPLE.query.filter(PCDB_SAMPLE.id==sample_id).first()
    oldsample_order = editsamps.sample_order
    oldsample_name = editsamps.sample_name
    if column_name == "sample_order":
        editsamps.sample_order = sample_order
        updated_value = sample_order
        original_value = oldsample_order
    if column_name == "sample_name":
        editsamps.sample_name = sample_name
        updated_value = sample_name
        original_value = oldsample_name
    db.session.commit()
    row_id = sample_id
    date_time = datetime.datetime.utcnow()
    change_type = "EDIT"
    table_name = "PCDB_SAMPLE"
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    db.session.commit()
    return ('', 204)

@app.route('/deleteeditsamps', methods=['GET','POST'])
def deleteeditsamps():
    sample_id = request.form.get('sample_id')
    parameter = PCDB_SAMPLE.query.filter(PCDB_SAMPLE.id==sample_id).first()
    sample_order = parameter.sample_order
    sample_name = parameter.sample_name
    db.session.delete(parameter)
    db.session.commit()
    row_id = sample_id
    date_time = datetime.datetime.utcnow()
    table_name = "PCDB_SAMPLE"
    change_type = "DELETE"
    updated_value = ""
    column_name = "sample_order"
    original_value = sample_order
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    column_name = "sample_name"
    original_value = sample_name
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    db.session.commit()
    return ('', 204)  

@app.route('/geteditallmeas', methods=['GET','POST'])
def geteditallmeas():
    editallmeas = []
    results = PCDB_MEASURE.query.with_entities(PCDB_MEASURE.id, PCDB_MEASURE.measure_name, PCDB_MEASURE.units_of_measure, PCDB_MEASURE.data_type).all()
    #Results are stored as a list of tuples - need to go through list and then unpack each tuple to get other data
    for result in results:
        measure_id = result[0]
        measure_name = result[1]
        units_of_measure = result[2]
        data_type = result[3]
        #This is needed to jsonify data - give each data point a title so page can interpret data   
        meas = {
                "measure_id": measure_id,
                "measure_name": measure_name,
                "units_of_measure": units_of_measure,
                "data_type": data_type
                }
        editallmeas.append(meas)
    return jsonify({'editallmeas':editallmeas})
    
@app.route('/updateeditmeas', methods=['GET','POST'])
def updateeditmeas():
    measure_id = request.form.get('sample_id')
    measure_name = request.form.get('measure_name')
    units_of_measure = request.form.get('units_of_measure')
    data_type = request.form.get("data_type")
    column_name = request.form.get('column_name')
    editmeas = PCDB_MEASURE.query.filter(PCDB_MEASURE.id==measure_id).first()
    oldmeasure_name = editmeas.measure_name
    oldunits_of_measure = editmeas.units_of_measure
    olddata_type = editmeas.data_type
    if column_name == "measure_name":
        editmeas.measure_name = measure_name
        updated_value = measure_name
        original_value = oldmeasure_name
    if column_name == "units_of_measure":
        editmeas.units_of_measure = units_of_measure
        updated_value = units_of_measure
        original_value = oldunits_of_measure
    if column_name == "data_type":
        editmeas.data_type = data_type
        updated_value = data_type
        original_value = olddata_type
    db.session.commit()
    row_id = measure_id
    date_time = datetime.datetime.utcnow()
    change_type = "EDIT"
    table_name = "PCDB_MEASURE"
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    db.session.commit()
    return ('', 204)

@app.route('/deleteeditmeas', methods=['GET','POST'])
def deleteeditmeas():	
    measure_id = request.form.get('measure_id')
    parameter = PCDB_MEASURE.query.filter(PCDB_MEASURE.id==measure_id).first()
    measure_name = parameter.measure_name
    units_of_measure = parameter.units_of_measure
    data_type = parameter.data_type
    db.session.delete(parameter)
    db.session.commit()
    row_id = measure_id
    date_time = datetime.datetime.utcnow()
    table_name = "PCDB_MEASURE"
    change_type = "DELETE"
    updated_value = ""
    column_name = "measure_name"
    original_value = measure_name
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    column_name = "units_of_measure"
    original_value = units_of_measure
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    column_name = "data_type"
    original_value = data_type
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    db.session.commit()
    return ('', 204)

@app.route('/getles', methods=['GET','POST'])
def getles():
    pro_id = request.form.get('pro_id') 
    process_id = pro_id.split(')')[0]
    les = []
    map_reps = []
    mapresults = PCDB_LES_V.query.with_entities(PCDB_LES_V.parameter_id).all()
    for mapresult in mapresults:
        map_reps.append(mapresult[0])
    results = PCDB_LES_V.query.with_entities(PCDB_LES_V.parameter_id, PCDB_LES_V.operation_name, PCDB_LES_V.operation_type, PCDB_LES_V.sample_name, PCDB_LES_V.measure_name, PCDB_LES_V.units_of_measure, PCDB_LES_V.sample_product, PCDB_LES_V.sample_stage, PCDB_LES_V.test_analysis,  PCDB_LES_V.mapping_id, PCDB_LES_V.adjustment_type, PCDB_LES_V.result_name, PCDB_LES_V.mapping_replicates, PCDB_LES_V.parameter_iteration).filter(PCDB_LES_V.process_id==process_id).all()
    for result in results:
        parameter_id = result[0]
        operation_name = result[1]
        operation_type = result[2]
        sample_name = result[3]
        measure_name = result[4]
        units_of_measure = result[5]
        sample_product = result[6]
        sample_stage = result[7]
        test_analysis = result[8]
        mapping_id = result[9]
        adjustment_type = result[10]
        result_name = result[11]
        str_mapping_replicates = result[12]
        parameter_iteration = result[13]
        params = {
                    "mapping_id": mapping_id,
                    "parameter_id": parameter_id, 
                    "operation_name": operation_name, 
                    "operation_type": operation_type, 
                    "adjustment_type": adjustment_type,
                    "sample_name": sample_name, 
                    "measure_name": measure_name, 
                    "units_of_measure": units_of_measure, 
                    "sample_product": sample_product,
                    "sample_stage": sample_stage,
                    "test_analysis": test_analysis,
                    "result_name": result_name,
                    "parameter_iteration": parameter_iteration,

                    }
        les.append(params)
        if not str_mapping_replicates:
            pass
        else:
            num_replicates = map_reps.count(parameter_id) - 1
            num_to_add = int(str_mapping_replicates) - num_replicates
            for x in range(0,num_to_add):
                params = {
                    "mapping_id": None,
                    "parameter_id": parameter_id, 
                    "operation_name": operation_name, 
                    "operation_type": operation_type, 
                    "adjustment_type": adjustment_type,
                    "sample_name": sample_name, 
                    "measure_name": measure_name, 
                    "units_of_measure": units_of_measure, 
                    "sample_product": sample_product,
                    "sample_stage": sample_stage,
                    "test_analysis": test_analysis,
                    "result_name": result_name,
                    "parameter_iteration": None,
                            }
                map_reps.append(parameter_id)
                les.append(params)
    return jsonify({'les':les})
    

@app.route('/updateles', methods=['GET','POST'])
def updateles():
    parameter_iteration = request.form.get('parameter_iteration')
    parameter_id = request.form.get('parameter_id')
    mapping_id = request.form.get('mapping_id')
    sample_product = request.form.get('sample_product')
    sample_stage = request.form.get('sample_stage')
    test_analysis = request.form.get('test_analysis')
    result_name = request.form.get('result_name')
    column_name = request.form.get('column_name')
    original_value = ""
    updated_value = ""
    if not mapping_id:   
        les = PCDB_LES(parameter_id,parameter_iteration,sample_product,sample_stage,test_analysis,result_name)
        db.session.add(les)
        db.session.commit()
        newles = PCDB_LES.query.filter(PCDB_LES.parameter_id==parameter_id).order_by(PCDB_LES.id.desc()).first() 
        mapping_id = newles.id
        column_name = "parameter_id"
        updated_value = parameter_id
        original_value = ""
        row_id = mapping_id
        date_time = datetime.datetime.utcnow()
        change_type = "ADD"
        table_name = "PCDB_LES"
        dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
        db.session.add(dbaudit)
        db.session.commit()
        les = PCDB_LES.query.filter(PCDB_LES.id==mapping_id).first()
        oldsample_product = les.sample_product
        oldsample_stage = les.sample_stage
        oldtest_analysis = les.test_analysis
        oldparameter_iteration = les.parameter_iteration
        oldresult_name = les.result_name
        if column_name == "parameter_iteration":
            les.parameter_iteration = parameter_iteration
            updated_value = parameter_iteration
            original_value = oldparameter_iteration
        if column_name == "sample_product":
            les.sample_product = sample_product
            updated_value = sample_product
            original_value = oldsample_product
        if column_name == "sample_stage":
            les.sample_stage = sample_stage
            updated_value = sample_stage
            original_value = oldsample_stage
        if column_name == "test_analysis":
            les.test_analysis = test_analysis
            updated_value = test_analysis
            original_value = oldtest_analysis
        if column_name == "result_name":
            les.result_name = result_name
            updated_value = result_name
            original_value = oldresult_name       
        db.session.commit()
        date_time = datetime.datetime.utcnow()
        change_type = "EDIT"
        table_name = "PCDB_LES"
        dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
        db.session.add(dbaudit)
        db.session.commit()
        return ('', 204)   
    else:
        les = PCDB_LES.query.filter(PCDB_LES.id==mapping_id).first()
        oldparameter_iteration = les.parameter_iteration
        oldsample_stage = les.sample_stage
        oldsample_product = les.sample_product
        oldtest_analysis = les.test_analysis
        oldresult_name = les.result_name
        if column_name == "parameter_iteration":
            les.parameter_iteration = parameter_iteration
            updated_value = parameter_iteration
            original_value = oldparameter_iteration
        if column_name == "sample_stage":
            les.sample_stage = sample_stage
            updated_value = sample_stage
            original_value = oldsample_stage
        if column_name == "sample_product":
            les.sample_product = sample_product
            updated_value = sample_product
            original_value = oldsample_product
        if column_name == "test_analysis":
            les.test_analysis = test_analysis
            updated_value = test_analysis
            original_value = oldtest_analysis
        if column_name == "result_name":
            les.result_name = result_name
            updated_value = result_name
            original_value = oldresult_name       
        db.session.commit()
        row_id = mapping_id
        date_time = datetime.datetime.utcnow()
        change_type = "EDIT"
        table_name = "PCDB_LES"
        dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
        db.session.add(dbaudit)
        db.session.commit()
        return ('', 204)   
    
@app.route('/deleteles', methods=['GET','POST'])
def deleteles():
    mapping_id = request.form.get('mapping_id')
    parameter_id = request.form.get('parameter_id')
    parameter_iteration = request.form.get('parameter_iteration')
    parameter = PCDB_LES.query.filter(PCDB_LES.id==mapping_id).first()
    sample_product = parameter.sample_product
    sample_stage = parameter.sample_stage
    test_analysis = parameter.test_analysis
    result_name = parameter.result_name
    db.session.delete(parameter)
    db.session.commit()    
    row_id = mapping_id
    date_time = datetime.datetime.utcnow()
    table_name = "PCDB_LES"
    change_type = "DELETE"
    updated_value = ""
    column_name = "sample_product"
    original_value = sample_product
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    column_name = "sample_stage"
    original_value = sample_stage
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    column_name = "test_analysis"
    original_value = test_analysis
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    column_name = "result_name"
    original_value = result_name
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    column_name = "parameter_iteration"
    original_value = parameter_iteration
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    column_name = "parameter_id"
    original_value = parameter_id
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    db.session.commit()
    return ('', 204)

@app.route('/getdvpi', methods=['GET','POST'])
def getdvpi():
    #ITS ADDING IT BEFORE ITS FINISHED THE LOOP SO IT ADDS ITS THEN FINDS THE NEXT PARAMETR AND ADDS IT AGAIN
    pro_id = request.form.get('pro_id')
    process_id = pro_id
    dvpis = []
    map_reps = []
    mapresults = PCDB_DVPI_V.query.with_entities(PCDB_DVPI_V.parameter_id).all()
    for mapresult in mapresults:
        map_reps.append(mapresult[0])
    results = PCDB_DVPI_V.query.with_entities(PCDB_DVPI_V.parameter_id, PCDB_DVPI_V.operation_name, PCDB_DVPI_V.operation_type, PCDB_DVPI_V.sample_name, PCDB_DVPI_V.measure_name, PCDB_DVPI_V.units_of_measure, PCDB_DVPI_V.parameter_iteration, PCDB_DVPI_V.unitprocedure, PCDB_DVPI_V.operation, PCDB_DVPI_V.phase, PCDB_DVPI_V.attribute, PCDB_DVPI_V.mapping_id, PCDB_DVPI_V.adjustment_type, PCDB_DVPI_V.mapping_replicates).filter(PCDB_DVPI_V.process_id==process_id).all()    
    for result in results:
        parameter_id = result[0]
        operation_name = result[1]
        operation_type = result[2]
        sample_name = result[3]
        measure_name = result[4]
        units_of_measure = result[5]
        parameter_iteration = result[6]
        unitprocedure = result[7]
        operation = result[8]
        phase = result[9]
        attribute = result[10]
        mapping_id = result[11]
        adjustment_type = result[12]
        str_mapping_replicates = result[13]
        params = {
                "parameter_id": parameter_id, 
                "mapping_id": mapping_id,
                "operation_name": operation_name, 
                "operation_type": operation_type, 
                "adjustment_type": adjustment_type,
                "sample_name": sample_name, 
                "measure_name": measure_name, 
                "units_of_measure": units_of_measure, 
                "parameter_iteration": parameter_iteration,
                "unitprocedure": unitprocedure,
                "operation": operation,
                "phase": phase,
                "attribute": attribute, 
                "str_mapping_replicates": str_mapping_replicates
                }
        dvpis.append(params)
        if not str_mapping_replicates:
            pass
        else:
            num_replicates = map_reps.count(parameter_id) - 1
            num_to_add = int(str_mapping_replicates) - num_replicates
            for x in range(0,num_to_add):
                params = {
                            "parameter_id": parameter_id, 
                            "mapping_id": None,
                            "operation_name": operation_name, 
                            "operation_type": operation_type,
                            "adjustment_type": adjustment_type,
                            "sample_name": sample_name, 
                            "measure_name": measure_name, 
                            "units_of_measure": units_of_measure, 
                            "parameter_iteration": None,
                            "unitprocedure": unitprocedure,
                            "operation": operation,
                            "phase": phase,
                            "attribute": attribute, 
                            "str_mapping_replicates": str_mapping_replicates
                            }
                map_reps.append(parameter_id)
                dvpis.append(params)
    return jsonify({'dvpis':dvpis})
    
@app.route('/updatedvpi', methods=['GET','POST'])
def updatedvpi():
    parameter_iteration = request.form.get('parameter_iteration')
    parameter_id = request.form.get('parameter_id')
    mapping_id = request.form.get('mapping_id')
    unitprocedure = request.form.get('unitprocedure')
    operation = request.form.get('operation')
    phase = request.form.get('phase')
    attribute = request.form.get('attribute')
    column_name = request.form.get('column_name')
    if not mapping_id:   
        dvpi = PCDB_DVPI(parameter_id,parameter_iteration,unitprocedure,operation,phase,attribute)
        db.session.add(dvpi)
        db.session.commit()
        newdvpi = PCDB_DVPI.query.filter(PCDB_DVPI.parameter_id==parameter_id).order_by(PCDB_DVPI.id.desc()).first() 
        mapping_id = newdvpi.id
        column_name = "parameter_id"
        updated_value = parameter_id
        original_value = ""
        row_id = mapping_id
        date_time = datetime.datetime.utcnow()
        change_type = "ADD"
        table_name = "PCDB_DVPI"
        dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
        db.session.add(dbaudit)
        db.session.commit()
        parameter_iteration = request.form.get('parameter_iteration')
        dvpi = PCDB_DVPI.query.filter(PCDB_DVPI.id==mapping_id).first()
        oldparameter_iteration = dvpi.parameter_iteration
        oldunitprocedure = dvpi.unitprocedure
        oldoperation = dvpi.operation
        oldphase = dvpi.phase
        oldattribute = dvpi.attribute
        if column_name == "parameter_iteration":
            dvpi.parameter_iteration = parameter_iteration
            updated_value = parameter_iteration
            original_value = oldparameter_iteration
        if column_name == "unitprocedure":
            dvpi.unitprocedure = unitprocedure
            updated_value = unitprocedure
            original_value = oldunitprocedure
        if column_name == "operation":
            dvpi.operation = operation
            updated_value = operation
            original_value = oldoperation
        if column_name == "phase":
            dvpi.phase = phase
            updated_value = phase
            original_value = oldphase
        if column_name == "attribute":
            dvpi.attribute = attribute
            updated_value = attribute
            original_value = oldattribute       
        db.session.commit()
        date_time = datetime.datetime.utcnow()
        change_type = "EDIT"
        table_name = "PCDB_DVPI"
        dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
        db.session.add(dbaudit)
        db.session.commit()
        return ('', 204)   
    else:
        parameter_iteration = request.form.get('parameter_iteration')
        unitprocedure = request.form.get('unitprocedure')
        operation = request.form.get('operation')
        phase = request.form.get('phase')
        attribute = request.form.get('attribute')
        column_name = request.form.get('column_name')
        dvpi = PCDB_DVPI.query.filter(PCDB_DVPI.id==mapping_id).first()
        oldparameter_iteration = dvpi.parameter_iteration
        oldunitprocedure = dvpi.unitprocedure
        oldoperation = dvpi.operation
        oldphase = dvpi.phase
        oldattribute = dvpi.attribute
        if column_name == "parameter_iteration":
            dvpi.parameter_iteration = parameter_iteration
            updated_value = parameter_iteration
            original_value = oldparameter_iteration
        if column_name == "unitprocedure":
            dvpi.unitprocedure = unitprocedure
            updated_value = unitprocedure
            original_value = oldunitprocedure
        if column_name == "operation":
            dvpi.operation = operation
            updated_value = operation
            original_value = oldoperation
        if column_name == "phase":
            dvpi.phase = phase
            updated_value = phase
            original_value = oldphase
        if column_name == "attribute":
            dvpi.attribute = attribute
            updated_value = attribute
            original_value = oldattribute       
        db.session.commit()
        row_id = mapping_id
        date_time = datetime.datetime.utcnow()
        change_type = "EDIT"
        table_name = "PCDB_DVPI"
        dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
        db.session.add(dbaudit)
        db.session.commit()
        return ('', 204)   

@app.route('/deletedvpi', methods=['GET','POST'])
def deletedvpi():
    mapping_id = request.form.get('mapping_id')
    parameter_id = request.form.get('parameter_id')
    parameter = PCDB_DVPI.query.filter(PCDB_DVPI.id==mapping_id).first()
    parameter_iteration = parameter.parameter_iteration
    if not parameter_iteration:
        parameter_iteration = ""
    unitprocedure = parameter.unitprocedure
    operation = parameter.operation
    phase = parameter.phase
    attribute = parameter.attribute
    db.session.delete(parameter)
    db.session.commit()    
    row_id = mapping_id
    date_time = datetime.datetime.utcnow()
    table_name = "PCDB_DVPI"
    change_type = "DELETE"
    updated_value = ""
    column_name = "parameter_id"
    original_value = parameter_id
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    column_name = "parameter_iteration"
    original_value = parameter_iteration
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    column_name = "unitprocedure"
    original_value = unitprocedure
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    column_name = "operation"
    original_value = operation
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    column_name = "phase"
    original_value = phase
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    column_name = "attribute"
    original_value = attribute
    dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
    db.session.add(dbaudit)
    db.session.commit()
    return ('', 204)   


@app.route('/editopsrow', methods=['GET','POST'])
def editopsrow():
    order_datas = request.args.getlist('op_order_list[]')
    for order_data in order_datas:
        operation_id = order_data.split("-")[0]
        old_operation_order = int(order_data.split("/")[0].split("-",1)[-1])
        new_operation_order = int(order_data.split("/",-1)[1]) + 1
        if new_operation_order == old_operation_order:
            pass
        else:
            opqry = PCDB_OPERATION.query.filter(PCDB_OPERATION.id==operation_id).first()
            opqry.operation_order = new_operation_order
            db.session.commit()
            date_time = datetime.datetime.utcnow()
            table_name = "PCDB_OPERATION"
            change_type = "EDIT"
            updated_value = new_operation_order
            original_value = old_operation_order
            column_name = "operation_order"
            row_id = operation_id
            dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
            db.session.add(dbaudit)
            db.session.commit()
    return ('', 204)

@app.route('/editsampsrow', methods=['GET','POST'])
def editsampsrow():
    order_datas = request.args.getlist('samp_order_list[]')
    for order_data in order_datas:
        sample_id = order_data.split("-")[0]
        old_sample_order = int(order_data.split("/")[0].split("-",1)[-1])
        new_sample_order = int(order_data.split("/",-1)[1]) + 1
        if new_sample_order == old_sample_order:
            pass
        else:
            opqry = PCDB_SAMPLE.query.filter(PCDB_SAMPLE.id==sample_id).first()
            opqry.sample_order = new_sample_order
            db.session.commit()
            date_time = datetime.datetime.utcnow()
            table_name = "PCDB_SAMPLE"
            change_type = "EDIT"
            updated_value = new_sample_order
            original_value = old_sample_order
            column_name = "sample_order"
            row_id = sample_id
            dbaudit = PCDB_AUDIT_TRAIL(date_time,change_type,table_name,row_id,column_name,original_value,updated_value)
            db.session.add(dbaudit)
            db.session.commit()
    return ('', 204)