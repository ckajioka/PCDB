from wtforms import TextField, validators, SelectField
from flask_wtf import FlaskForm

class pcdb_new_product_form(FlaskForm):
    biib_code_new = TextField('biib_code', validators=[validators.DataRequired()])
    partner = SelectField('partner', choices=[('Biogen','Biogen'),('Bioverativ','Bioverativ'),('Janssen','Janssen'),('Samsung','Samsung'),('Celgene','Celgene'),('Vir','Vir'),('Eisai','Eisai')], validators=[validators.DataRequired()])
    modality = SelectField('modality', choices=[('Protein','Protein'),('Small Molecule','Small Molecule'),('ASO','ASO'),('Gene Therapy','Gene Therapy')], validators=[validators.DataRequired()])
     
class pcdb_new_product_alias_form(FlaskForm):
    biib_code_alias = TextField('biib_code', validators=[validators.DataRequired()])
    alias = TextField('alias', validators=[validators.DataRequired()])
    alias_type = SelectField('alias_type', 
                             choices=[('Alternate Name','Alternate Name'),('Molecule Name','Molecule Name'),('Partner Name','Partner Name'),('Smart Batch Code','Smart Batch Code'),('Target Protein','Target Protein'),('Trade Name','Trade Name')])

class pcdb_new_product_process_form(FlaskForm):
    mfg_mode = SelectField('MFG Mode', choices=[('DS','DS'),('API','API'),('SMDP','SMDP'),('LMDP','LMDP'),('ASO','ASO')], validators=[validators.DataRequired()])
    process_identifier = TextField('Process Identifier', validators=[validators.DataRequired()])
    process_description = TextField('Description')
    process_status = SelectField('Process Status', choices=[('ACTIVE','ACTIVE'),('RETIRED','RETIRED')], validators=[validators.DataRequired()])

class pcdb_new_operation_form(FlaskForm):
    operation_order = TextField('Operation Order', validators=[validators.DataRequired()])
    operation_name = TextField('Operation Name', validators=[validators.DataRequired()])
    operation_type = TextField('Operation Type', validators=[validators.DataRequired()])
    chrom_type = SelectField('Chrom Type', choices=[('NULL','-Chrom Type-'),('NULL','N/A'),('BINDELUTE','BINDELUTE'),('FLOWTHROUGH','FLOWTHROUGH')])
    adjustment_type = SelectField('Adjustment Type', choices=[('NULL','-Adjustment Type-'),('NULL','N/A'),('pH Up','pH Up'),('pH Down','pH Down'),('pH Up/Down','pH Up/Down'), ('Conductivity Up','Conductivity Up'),('Conductivity Up/Down','Conductivity Up/Down'),('Surfactant', 'Surfactant'),('Formulation','Formulation')])
    adjustment_alias = TextField('BPR Order')
    cycle_pool = SelectField('Cycle/Pool', choices=[('NULL','-Cycle/Pool-'),('NULL','N/A'),('PER_CYCLE','PER_CYCLE'),('POOL','POOL')])
    bpr_order = TextField('BPR Order')

class pcdb_new_sample_form(FlaskForm):
    sample_order = TextField('Sample Order', validators=[validators.DataRequired()])
    sample_name = TextField('Sample Name', validators=[validators.DataRequired()])

class pcdb_new_measure_form(FlaskForm):
    measure_name = TextField('Sample Name', validators=[validators.DataRequired()])
    units_of_measure = TextField('Sample Name', validators=[validators.DataRequired()])
    data_type = SelectField('Data Type', choices=[('NULL','-Data Type-'),('string','string'),('numeric','numeric'),('DateTime','DateTime')], validators=[validators.DataRequired()])
    